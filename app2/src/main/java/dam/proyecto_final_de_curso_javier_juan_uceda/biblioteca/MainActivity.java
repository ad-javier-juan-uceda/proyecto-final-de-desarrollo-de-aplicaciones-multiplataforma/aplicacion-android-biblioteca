package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.UsuarioDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    public static final String MIS_PREFERENCIAS = "Preferencias2";
    public static Usuario socio_logueado = null;
    public static SharedPreferences misPreferencias;
    private TextView tvnombreusuario;
    private TextView tvpantallasesion;
    private TextView tvcontrasenya;
    private EditText ednombredeusuario;
    private EditText edcontrasenya;
    private Button btcontrasenya;
    private ProgressBar progressBar;
    private inicio_sesion inicio_sesion;
    private ImageView contrasenya;
    public static final int NUMERO_NEUTRO = 0;
    private CheckBox checkbox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUI();
        createNotificationChannel();
    }

    public static void escribir_en_preferencias(int idUsuario) {
        SharedPreferences.Editor editor = misPreferencias.edit();
        editor.putInt("recordarusuario", idUsuario);
        editor.commit();
    }

    public static int leer_en_preferencias() {
        return misPreferencias.getInt("recordarusuario", NUMERO_NEUTRO);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    private void setUI() {

        tvnombreusuario = findViewById(R.id.tvnombreusuario);
        tvpantallasesion = findViewById(R.id.tvpantallasesion);
        tvcontrasenya = findViewById(R.id.tvcontrasenya);
        ednombredeusuario = findViewById(R.id.ednombredeusuario);
        edcontrasenya = findViewById(R.id.edContrasenya);
        btcontrasenya = findViewById(R.id.btcontrasenya);
        btcontrasenya.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
        contrasenya = findViewById(R.id.imagencontrasenyacambiar1);
        checkbox = findViewById(R.id.checkBox);
        checkbox.setOnCheckedChangeListener(this);
        misPreferencias = getSharedPreferences(MIS_PREFERENCIAS, MODE_PRIVATE);

        int idusuario = leer_en_preferencias();

        if (idusuario > NUMERO_NEUTRO) {

            Thread thread = new Thread(() -> {
                try {
                    Usuario usuario;
                    if (new MainActivity().isNetworkAvailable()) {
                        usuario = new UsuarioDAO().buscarPorClavePrimaria(idusuario);
                        if (usuario != null) {
                            socio_logueado = usuario;
                            runOnUiThread(() -> cambiar_pantalla());
                        }
                    } else {
                        crear_toast(getString(R.string.no_hay_conexion), false);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (inicio_sesion != null) {
            cancelariniciosesion();
            Log.e("onDestroy()", "ASYNCTASK was cancelled");
        } else {
            Log.e("onDestroy()", "ASYNCTASK = NULL, was not cancelled");
        }
    }


    private void iniciarsesion() {

        if (isNetworkAvailable()) {
            String contrasenya = edcontrasenya.getText().toString();
            String nombre_usuario = ednombredeusuario.getText().toString();
            try {
                if (!contrasenya.isEmpty()) {
                    if (!nombre_usuario.isEmpty()) {
                        cancelariniciosesion();
                        inicio_sesion = new inicio_sesion();
                        inicio_sesion.execute(nombre_usuario, contrasenya);

                    } else {
                        crear_toast(getString(R.string.nombre_usuario_vacio), false);
                    }
                } else {
                    crear_toast(getString(R.string.contrasenya_vacia), false);
                }
            } catch (Exception ex) {
                crear_toast(ex.getMessage(), false);
            }
        } else {
            crear_toast(getString(R.string.no_hay_conexion), false);
        }


    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void cancelariniciosesion() {
        progressBar.setVisibility(View.INVISIBLE);
        AsyncTask.Status status;
        if (inicio_sesion != null) {
            status = inicio_sesion.getStatus();
            Log.i("cancel", "Status: " + status);
            if ((inicio_sesion != null) && ((status == AsyncTask.Status.RUNNING) || (status == AsyncTask.Status.PENDING))) {

                Log.i("cancel", "The buton has pressed while the task is running");
                inicio_sesion.cancel(true);
                Log.i("cancel", "The task have just cancelled.");
            }
        } else Log.i("cancel", "Not started");

    }

    public void crear_toast(final String text_to_print, final boolean acierto) {

        runOnUiThread(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate((acierto ? R.layout.toast_personalizado_acierto : R.layout.toast_personalizado_error), findViewById(R.id.toast_personalizado));

            TextView text = layout.findViewById(R.id.textView);
            text.setText(text_to_print);

            android.widget.Toast toast = new android.widget.Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(android.widget.Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        });


    }

    public void cambiar_progressbar(final boolean visible) {

        runOnUiThread(() -> progressBar.setVisibility(visible ? View.VISIBLE : View.INVISIBLE));


    }

    public void cambiar_pantalla() {

        runOnUiThread(() -> {
            Intent i = new Intent(getApplicationContext(), recyclerViewLibros.class);
            startActivity(i);
            overridePendingTransition(R.anim.left_in, R.anim.left_out);
        });


    }


    public boolean isNetworkAvailable() {
        boolean networkAvailable = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                networkAvailable = true;
            }
        }

        return networkAvailable;

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btcontrasenya:
                iniciarsesion();
            default:
        }
    }

    public void mostrar_contrasenya(View view) {
        edcontrasenya.setInputType(edcontrasenya.getInputType() == 129 ? InputType.TYPE_CLASS_TEXT : 129);
        contrasenya.setImageResource(edcontrasenya.getInputType() == 129 ? android.R.drawable.ic_menu_edit : android.R.drawable.ic_menu_view);
    }

    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("teclado", edcontrasenya.getInputType());
        outState.putString("nombre_usuario", ednombredeusuario.getText().toString());
        outState.putString("contrasenya", edcontrasenya.getText().toString());
        outState.putBoolean("recordar", checkbox.isChecked());
    }

    protected void escribir_usuario() {
        runOnUiThread(() -> {
            if (checkbox.isChecked()) {
                escribir_en_preferencias(socio_logueado.getId());
            }
        });

    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        edcontrasenya.setInputType(savedInstanceState.getInt("teclado", 129));
        ednombredeusuario.setText(savedInstanceState.getString("nombre_usuario", ""));
        edcontrasenya.setText(savedInstanceState.getString("contrasenya", ""));
        checkbox.setChecked(savedInstanceState.getBoolean("recordar", false));
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (!isChecked) {
            int idusuario = leer_en_preferencias();
            if (idusuario > NUMERO_NEUTRO) {
                escribir_en_preferencias(NUMERO_NEUTRO);
            }
        }
    }

    private class inicio_sesion extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... textos) {
            cambiar_progressbar(true);
            Usuario usuario;
            try {
                usuario = new UsuarioDAO().Iniciar_sesion(textos[0], textos[1]);

                if (usuario != null) {
                    socio_logueado = usuario;

                    escribir_usuario();

                    cambiar_pantalla();
                } else {
                    crear_toast(getString(R.string.logueado_incorrecto), false);
                }

            } catch (Exception e) {
                crear_toast(e.getMessage(), false);
                cambiar_progressbar(false);
            }

            return null;
        }

        protected void onProgressUpdate(Integer... progress) {
            cambiar_progressbar(true);
        }

        protected void onPostExecute(Integer result) {
            cambiar_progressbar(false);
        }
    }
}
