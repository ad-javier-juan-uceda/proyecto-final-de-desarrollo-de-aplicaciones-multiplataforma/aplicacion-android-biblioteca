package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads.ThreadLibro;

public class Inicio extends AppCompatActivity {
    private static final long espera = 10 * 1000;
    private ProgressBar pbaplicacion;

    public void crear_toast(final String text_to_print, final boolean acierto) {

        runOnUiThread(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate((acierto ? R.layout.toast_personalizado_acierto : R.layout.toast_personalizado_error), findViewById(R.id.toast_personalizado));

            TextView text = layout.findViewById(R.id.textView);
            text.setText(text_to_print);

            android.widget.Toast toast = new android.widget.Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(android.widget.Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        });


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicio);
        pbaplicacion = findViewById(R.id.pbaplicacion);
        int limite = 0;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            limite = (int) espera;
        }
        if (isNetworkAvailable()) {
            ThreadLibro threadLibro = new ThreadLibro();
            threadLibro.start();
            // Using handler with postDelayed called runnable run method
            int finalLimite = limite;
            pbaplicacion.setMax(limite);
            Thread thread = new Thread(() ->
            {
                for (int contador = 0; contador < finalLimite; contador++) {
                    pbaplicacion.setProgress(contador);
                    try {
                        Thread.sleep(1);
                    } catch (InterruptedException e) {

                    }
                }
                pbaplicacion.setProgress(0);
                pbaplicacion.setVisibility(View.INVISIBLE);
            });
            thread.start();
            new Handler().postDelayed(() -> {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    Intent i = new Intent(getApplicationContext(), MainActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    overridePendingTransition(R.anim.left_in, R.anim.left_out);

                } else {
                    Intent i = new Intent(Inicio.this, MainActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }


            }, espera);
        } else {

            try {
                crear_toast(getString(R.string.no_hay_conexion), false);
                Thread.sleep(3000);
                finish();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }


    }

    public boolean isNetworkAvailable() {
        boolean networkAvailable = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                networkAvailable = true;
            }
        }

        return networkAvailable;

    }

}
