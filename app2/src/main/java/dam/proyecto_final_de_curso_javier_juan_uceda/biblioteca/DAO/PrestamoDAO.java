package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Prestamo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Socio;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class PrestamoDAO implements GenericoDAO<Prestamo> {

    private XmlRpcClient Connection;
    private LibroDAO libroDAO;
    private SocioDAO socioDAO;

    private static final String ESTADO_DEL_LIBRO = "estado";
    private static final String FECHA_DE_DEVOLUCION = "fecha_de_devolucion";
    private static final String FECHA_DE_DEVUELTO = "fecha_de_devuelto";
    private static final String FECHA_DE_PRESTAMO = "fecha_de_prestamo";
    private static final String SOCIO = "socio_id";
    private static final String LIBRO = "libro_id";

    private static final String NOMBRE_TABLA = "biblioteca.prestamo";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public PrestamoDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
        libroDAO = new LibroDAO();
        socioDAO = new SocioDAO();
    }

    @Override
    public ArrayList<Prestamo> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, FECHA_DE_DEVOLUCION, FECHA_DE_DEVUELTO, FECHA_DE_PRESTAMO, SOCIO, LIBRO));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Prestamo buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, FECHA_DE_DEVOLUCION, FECHA_DE_DEVUELTO, FECHA_DE_PRESTAMO, SOCIO, LIBRO));
                    }
                }
        )));
        ArrayList<Prestamo> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Prestamo> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, FECHA_DE_DEVOLUCION, FECHA_DE_DEVUELTO, FECHA_DE_PRESTAMO, SOCIO, LIBRO));
                    }
                }
        )));
        ArrayList<Prestamo> lista = obtenerLista(list);

        return lista;
    }

    @Override
    public Integer insertarRegistro(Prestamo objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(FECHA_DE_DEVOLUCION, DAO.fecha_correcta(objeto.getFecha_que_se_tiene_que_devolver()));
                        put(FECHA_DE_DEVUELTO, DAO.fecha_correcta(objeto.getFecha_que_se_ha_devuelto()));
                        put(FECHA_DE_PRESTAMO, DAO.fecha_correcta(objeto.getFecha_de_prestamo()));
                        put(SOCIO, objeto.getSocio().getId());
                        put(LIBRO, objeto.getLibro_prestado().getId());

                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Prestamo objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(FECHA_DE_DEVOLUCION, DAO.fecha_correcta(objeto.getFecha_que_se_tiene_que_devolver()));
                                put(FECHA_DE_DEVUELTO, DAO.fecha_correcta(objeto.getFecha_que_se_ha_devuelto()));
                                put(FECHA_DE_PRESTAMO, DAO.fecha_correcta(objeto.getFecha_de_prestamo()));
                                put(SOCIO, objeto.getSocio().getId());
                                put(LIBRO, objeto.getLibro_prestado().getId());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Prestamo objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Prestamo> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Prestamo> lista = new ArrayList();
        Prestamo autor;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            autor = new Prestamo();
            elemento_actual = (HashMap) elemento;

            autor.setId((int) elemento_actual.get(ID));
            autor.setSocio((Socio) DAO.Relacion_uno_a_muchos(elemento_actual, SOCIO, SocioDAO.class.getName()));
            autor.setLibro_prestado((Libro) DAO.Relacion_uno_a_muchos(elemento_actual, LIBRO, LibroDAO.class.getName()));
            autor.setFecha_de_prestamo(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_PRESTAMO)));
            autor.setFecha_que_se_ha_devuelto(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_DEVUELTO)));
            autor.setFecha_que_se_tiene_que_devolver(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_DEVOLUCION)));

            lista.add(autor);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Prestamo> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

}
