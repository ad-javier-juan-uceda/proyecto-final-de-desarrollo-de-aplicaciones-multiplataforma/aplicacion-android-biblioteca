package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import android.os.Build;

import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Retirada;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Socio;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class RetiradaDAO implements GenericoDAO<Retirada> {

    private static final String ESTADO_DEL_LIBRO = "motivo";
    private XmlRpcClient Connection;
    private SocioDAO socioDAO;
    private static final String FECHA_DE_FINALIZACION_DE_RETIRADA = "fecha_de_finalizacion_retirada";
    private static final String FECHA_DE_INICIO_RETIRADA = "fecha_de_inicio_retirada";
    private static final String SOCIO = "socio_id";

    private static final String NOMBRE_TABLA = "biblioteca.retirada";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public RetiradaDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
        socioDAO = new SocioDAO();
    }

    @Override
    public ArrayList<Retirada> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, ESTADO_DEL_LIBRO, FECHA_DE_FINALIZACION_DE_RETIRADA, FECHA_DE_INICIO_RETIRADA, SOCIO));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Retirada buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, ESTADO_DEL_LIBRO, FECHA_DE_FINALIZACION_DE_RETIRADA, FECHA_DE_INICIO_RETIRADA, SOCIO));
                    }
                }
        )));
        ArrayList<Retirada> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Retirada> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, ESTADO_DEL_LIBRO, FECHA_DE_FINALIZACION_DE_RETIRADA, FECHA_DE_INICIO_RETIRADA, SOCIO));
                    }
                }
        )));
        ArrayList<Retirada> lista = obtenerLista(list);

        return lista;
    }

    @Override
    public Integer insertarRegistro(Retirada objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(ESTADO_DEL_LIBRO, objeto.getEstado_del_libro());
                        put(FECHA_DE_FINALIZACION_DE_RETIRADA, DAO.fecha_correcta(objeto.getFecha_que_se_devuelve_el_carnet()));
                        put(FECHA_DE_INICIO_RETIRADA, DAO.fecha_correcta(objeto.getFecha_que_se_retira_el_carnet()));
                        put(SOCIO, objeto.getSocio().getId());

                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Retirada objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(ESTADO_DEL_LIBRO, objeto.getEstado_del_libro());
                                put(FECHA_DE_FINALIZACION_DE_RETIRADA, DAO.fecha_correcta(objeto.getFecha_que_se_devuelve_el_carnet()));
                                put(FECHA_DE_INICIO_RETIRADA, DAO.fecha_correcta(objeto.getFecha_que_se_retira_el_carnet()));
                                put(SOCIO, objeto.getSocio().getId());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Retirada objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Retirada> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Retirada> lista = new ArrayList();
        Retirada autor;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            autor = new Retirada();
            elemento_actual = (HashMap) elemento;

            autor.setId((int) elemento_actual.get(ID));
            autor.setEstado_del_libro(DAO.transformar_texto(elemento_actual.get(ESTADO_DEL_LIBRO)));
            autor.setFecha_que_se_devuelve_el_carnet(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_FINALIZACION_DE_RETIRADA)));
            autor.setFecha_que_se_retira_el_carnet(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_INICIO_RETIRADA)));
            autor.setSocio((Socio) DAO.Relacion_uno_a_muchos(elemento_actual, SOCIO, SocioDAO.class.getName()));
            lista.add(autor);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Retirada> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public Retirada buscar(Retirada objeto) throws Exception {
        Retirada encontrado = null;
        ArrayList<Retirada> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (lista.get(i).equals(objeto)) {
                    i = lista.size();
                }
            }
        }
        return encontrado;
    }

}
