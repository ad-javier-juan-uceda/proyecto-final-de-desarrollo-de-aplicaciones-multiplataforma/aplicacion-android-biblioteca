package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;


import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Autor;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Editorial;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Localizacion;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Tipo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class LibroDAO implements GenericoDAO<Libro> {


    private XmlRpcClient Connection;
    private EditorialDAO editorialDAO;
    private LocalizacionDAO localizacionDAO;
    private TipoDAO tipoDAO;
    private AutorDAO autorDAO;

    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;
    private static final String NOMBRE_TABLA = "biblioteca.libro";

    private static final String TITULO = "titulo";
    private static final String ESTADO = "estado";
    private static final String PAGINAS = "paginas";
    private static final String PRESTADO = "prestado";
    private static final String EDITORIAL = "editorial_id";
    private static final String TIPO = "tipo_id";
    private static final String ISBN = "isbn";
    private static final String CANTIDAD = "cantidad";
    private static final String AUTOR = "autor_id";
    private static final String LOCALIZACION = "localizacion_id";

    public LibroDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
        editorialDAO = new EditorialDAO();
        localizacionDAO = new LocalizacionDAO();
        tipoDAO = new TipoDAO();
        autorDAO = new AutorDAO();
    }

    @Override
    public ArrayList<Libro> buscarTodos() throws Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, TITULO, ESTADO, PAGINAS, PRESTADO, EDITORIAL, TIPO, ISBN, CANTIDAD, AUTOR, LOCALIZACION));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Libro buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, TITULO, ESTADO, PAGINAS, PRESTADO, EDITORIAL, TIPO, ISBN, CANTIDAD, AUTOR, LOCALIZACION));
                    }
                }
        )));
        ArrayList<Libro> lista = obtenerLista(list);
        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Libro> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, TITULO, ESTADO, PAGINAS, PRESTADO, EDITORIAL, TIPO,
                                ISBN, CANTIDAD, AUTOR, LOCALIZACION));
                    }
                }
        )));
        ArrayList<Libro> lista = obtenerLista(list);
        return lista;
    }

    @Override
    public Integer insertarRegistro(Libro objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(TITULO, objeto.getTitulo());
                        put(ESTADO, objeto.getEstado_del_libro());
                        put(PAGINAS, objeto.getPaginas());
                        put(PRESTADO, objeto.isEsta_prestado());
                        put(EDITORIAL, objeto.getEditorial().getId());
                        put(TIPO, objeto.getTipo_del_libro().getId());
                        put(ISBN, objeto.getISBN());
                        put(CANTIDAD, objeto.getCantidad());
                        put(AUTOR, objeto.getAutor().getId());
                        put(LOCALIZACION, objeto.getLocalizacion().getId());

                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Libro objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(
                        asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(TITULO, objeto.getTitulo());
                                put(ESTADO, objeto.getEstado_del_libro());
                                put(PAGINAS, objeto.getPaginas());
                                put(PRESTADO, objeto.isEsta_prestado());
                                put(EDITORIAL, objeto.getEditorial().getId());
                                put(TIPO, objeto.getTipo_del_libro().getId());
                                put(ISBN, objeto.getISBN());
                                put(CANTIDAD, objeto.getCantidad());
                                put(AUTOR, objeto.getAutor().getId());
                                put(LOCALIZACION, objeto.getLocalizacion().getId());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Libro objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Libro> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Libro> lista = new ArrayList();
        Libro libro;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            libro = new Libro();
            elemento_actual = (HashMap) elemento;

            libro.setId((int) elemento_actual.get(ID));
            libro.setTitulo(DAO.transformar_texto(elemento_actual.get(TITULO)));
            libro.setISBN(DAO.transformar_texto(elemento_actual.get(ISBN)));
            libro.setEstado_del_libro(DAO.transformar_texto(elemento_actual.get(ESTADO)));
            libro.setCantidad((int) elemento_actual.get(CANTIDAD));
            libro.setPaginas((int) elemento_actual.get(PAGINAS));
            libro.setEsta_prestado((boolean) elemento_actual.get(PRESTADO));
            libro.setEditorial((Editorial) DAO.Relacion_uno_a_muchos(elemento_actual, EDITORIAL, EditorialDAO.class.getName()));
            libro.setLocalizacion((Localizacion) DAO.Relacion_uno_a_muchos(elemento_actual, LOCALIZACION, LocalizacionDAO.class.getName()));
            libro.setTipo_del_libro((Tipo) DAO.Relacion_uno_a_muchos(elemento_actual, TIPO, TipoDAO.class.getName()));
            libro.setAutor((Autor) DAO.Relacion_uno_a_muchos(elemento_actual, AUTOR, AutorDAO.class.getName()));
            lista.add(libro);
        }
        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Libro> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public Libro buscar(Libro objeto) throws Exception {
        Libro encontrado = null;
        ArrayList<Libro> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(objeto)) {
                i = lista.size();
            }
        }
        return encontrado;
    }

    public ArrayList<Libro> buscarMasDeUno(ArrayList<Libro> objeto) throws Exception {
        ArrayList<Libro> encontrado = new ArrayList<>();
        ArrayList<Libro> lista = buscarTodos();

        for (int i = 0; i < lista.size(); i++) {
            encontrado.add(buscar(lista.get(i)));
        }

        return objeto;
    }


}
