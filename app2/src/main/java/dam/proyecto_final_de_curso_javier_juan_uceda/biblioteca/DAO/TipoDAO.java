package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import android.os.Build;

import androidx.annotation.RequiresApi;

import org.GenericoDAO.GenericoDAO;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Tipo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class TipoDAO implements GenericoDAO<Tipo> {

    private final XmlRpcClient Connection;

    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;
    private static final String NOMBRE_TABLA = "biblioteca.tipo";

    private static final String NOMBRE = Constantes.NOMBRE_TIPO;
    private static final String DESCRIPCION = Constantes.DESCRIPCION_TIPO;

    public TipoDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
    }

    @Override
    public ArrayList<Tipo> buscarTodos() throws Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, DESCRIPCION));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Tipo buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, DESCRIPCION));
                    }
                }
        )));
        ArrayList<Tipo> lista = obtenerLista(list);
        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Tipo> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;
        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, DESCRIPCION));
                    }
                }
        )));
        ArrayList<Tipo> lista = obtenerLista(list);
        return lista;
    }

    @Override
    public Integer insertarRegistro(Tipo objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DESCRIPCION, objeto.getDescripcion());
                    }
                })
        ));

        return id;
    }

    public Integer insertarRegistroRestaurar(Tipo objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(DESCRIPCION, objeto.getDescripcion());
                    }
                })
        ));

        return id;
    }

    public Integer[] insertarMasDeUnRegistroRestaurar(ArrayList<Tipo> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistroRestaurar(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @Override
    public boolean actualizarRegistro(Tipo objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(
                        asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(NOMBRE, objeto.getNombre());
                                put(DESCRIPCION, objeto.getDescripcion());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Tipo objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Tipo> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Tipo> lista = new ArrayList();
        Tipo libro;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            libro = new Tipo();
            elemento_actual = (HashMap) elemento;

            libro.setId((int) elemento_actual.get(ID));
            libro.setNombre(DAO.transformar_texto(elemento_actual.get(NOMBRE)));
            libro.setDescripcion(DAO.transformar_texto(elemento_actual.get(DESCRIPCION)));
            lista.add(libro);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Tipo> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public Tipo buscar(Tipo objeto) throws Exception {
        Tipo encontrado = null;
        ArrayList<Tipo> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (lista.get(i).equals(objeto)) {
                i = lista.size();
            }
        }
        return encontrado;
    }

}
