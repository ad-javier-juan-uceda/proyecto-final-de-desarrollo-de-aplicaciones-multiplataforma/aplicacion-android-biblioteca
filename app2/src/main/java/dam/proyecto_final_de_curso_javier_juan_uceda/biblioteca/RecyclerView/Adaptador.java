package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.R;

public class Adaptador extends RecyclerView.Adapter<Adaptador.MyViewHolder> {

    private ArrayList<Libro> myDataset;
    private OnItemClickListener listener;
    private Context context;

    public Adaptador(Context context, ArrayList<Libro> myDataset, OnItemClickListener listener) {
        this.myDataset = myDataset;
        this.listener = listener;
        this.context = context;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // inflate item_layout
        View itemLayoutView = LayoutInflater.from(context)
                .inflate(R.layout.activity_adaptador, parent, false);

        MyViewHolder vh = new MyViewHolder(itemLayoutView);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myviewholder, int position) {
        myviewholder.bind(myDataset.get(position), listener);
    }

    @Override
    public int getItemCount() {
        return myDataset.size();
    }

    public interface OnItemClickListener {
        void Click(Libro activityName);
    }

    static public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView titulo_libro;
        TextView isbn_libro;
        TextView estado_del_libro;
        TextView cantidad_libro;
        TextView paginas_del_libro;
        TextView tipo_del_libro;
        TextView autor_del_libro;
        ImageView imageView2;
        ConstraintLayout constraintLayout;

        public MyViewHolder(@NonNull View vista) {
            super(vista);
            titulo_libro = vista.findViewById(R.id.titulo_libro);
            isbn_libro = vista.findViewById(R.id.isbn_libro);
            estado_del_libro = vista.findViewById(R.id.estado_del_libro);
            cantidad_libro = vista.findViewById(R.id.cantidad_libro);
            paginas_del_libro = vista.findViewById(R.id.paginas_del_libro);
            tipo_del_libro = vista.findViewById(R.id.tipo_del_libro);
            autor_del_libro = vista.findViewById(R.id.autor_del_libro);
            imageView2 = vista.findViewById(R.id.imageView2);
            constraintLayout = vista.findViewById(R.id.constraint);
        }

        public void bind(Libro libro, OnItemClickListener listener) {
            titulo_libro.setText(libro.getTitulo());
            titulo_libro.setOnClickListener(v -> listener.Click(libro));

            isbn_libro.setText(libro.getISBN());
            isbn_libro.setOnClickListener(v -> listener.Click(libro));

            int cantidad = libro.getCantidad();
            String texto_cantidad = "";
            if (cantidad > 1) {
                texto_cantidad = "Queda " + cantidad + " libro";
            } else {
                texto_cantidad = "Quedan " + cantidad + " libros";
            }

            estado_del_libro.setText(libro.getEstado_del_libro());
            estado_del_libro.setOnClickListener(v -> listener.Click(libro));

            cantidad_libro.setText(texto_cantidad);
            cantidad_libro.setOnClickListener(v -> listener.Click(libro));

            int paginas = libro.getCantidad();
            String texto_paginas = "";
            if (paginas > 1) {
                texto_paginas = "Queda " + paginas + " pagina";
            } else {
                texto_paginas = "Quedan " + paginas + " paginas";
            }

            paginas_del_libro.setText(texto_paginas);
            paginas_del_libro.setOnClickListener(v -> listener.Click(libro));

            tipo_del_libro.setText(libro.getTipo_del_libro().getNombre());
            tipo_del_libro.setOnClickListener(v -> listener.Click(libro));

            autor_del_libro.setText(libro.getAutor().getNombre());
            autor_del_libro.setOnClickListener(v -> listener.Click(libro));

            constraintLayout.setOnClickListener(v -> listener.Click(libro));

            imageView2.setImageResource(libro.isEsta_prestado() ? android.R.drawable.star_big_on : android.R.drawable.star_big_off);

        }

    }


}
