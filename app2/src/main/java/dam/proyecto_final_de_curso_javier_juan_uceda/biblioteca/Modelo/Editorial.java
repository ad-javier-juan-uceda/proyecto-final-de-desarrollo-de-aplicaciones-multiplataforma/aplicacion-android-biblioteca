package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo;


import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Objects;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;

public class Editorial {

    private int id;
    private String nombre;
    private int telefono;
    private String direccion;
    private String localidad;
    private String CIF;
    private String pagina_web;
    private String facebook;
    private String twitter;
    private String instagram;

    public Editorial(String nombre, int telefono, String direccion, String localidad, String CIF, String pagina_web, String facebook, String twitter, String instagram) {
        this.nombre = nombre;
        this.telefono = telefono;
        this.direccion = direccion;
        this.localidad = localidad;
        this.CIF = CIF;
        this.pagina_web = pagina_web;
        this.facebook = facebook;
        this.twitter = twitter;
        this.instagram = instagram;
    }

    public Editorial() {
        this.nombre = Constantes.VACIO;
        this.telefono = 0;
        this.direccion = Constantes.VACIO;
        this.localidad = Constantes.VACIO;
        this.CIF = Constantes.VACIO;
        this.pagina_web = Constantes.VACIO;
        this.facebook = Constantes.VACIO;
        this.twitter = Constantes.VACIO;
        this.instagram = Constantes.VACIO;
        this.id = 0;
    }

    public Editorial(Editorial editorial) {
        this.nombre = editorial.getNombre();
        this.telefono = editorial.getTelefono();
        this.direccion = editorial.getDireccion();
        this.localidad = editorial.getLocalidad();
        this.CIF = editorial.getCIF();
        this.pagina_web = editorial.getPagina_web();
        this.facebook = editorial.getFacebook();
        this.twitter = editorial.getTwitter();
        this.instagram = editorial.getInstagram();
        id = editorial.getId();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getCIF() {
        return CIF;
    }

    public void setCIF(String CIF) {
        this.CIF = CIF;
    }

    public String getPagina_web() {
        return pagina_web;
    }

    public void setPagina_web(String pagina_web) {
        this.pagina_web = pagina_web;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Editorial other = (Editorial) obj;
        if (this.telefono != other.telefono) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        if (!Objects.equals(this.localidad, other.localidad)) {
            return false;
        }
        if (!Objects.equals(this.CIF, other.CIF)) {
            return false;
        }
        if (!Objects.equals(this.pagina_web, other.pagina_web)) {
            return false;
        }
        if (!Objects.equals(this.facebook, other.facebook)) {
            return false;
        }
        if (!Objects.equals(this.twitter, other.twitter)) {
            return false;
        }
        return Objects.equals(this.instagram, other.instagram);
    }

    public boolean equals(Editorial objeto) {

        boolean igual = true;
        try {
            if (!(
                    objeto.getFacebook().equals(this.getFacebook())
                            && objeto.getTwitter().equals(this.getTwitter())
                            && objeto.getInstagram().equals(this.getInstagram())
                            && objeto.getDireccion().equals(this.getDireccion())
                            && objeto.getLocalidad().equals(this.getLocalidad())
                            && objeto.getCIF().equals(this.getCIF())
                            && objeto.getNombre().equals(this.getNombre())
                            && objeto.getPagina_web().equals(this.getPagina_web())
                            && objeto.getTelefono() == this.getTelefono()
            )) {
                throw new Exception();
            }
        } catch (Exception e) {
            igual = false;
        }
        return igual;

    }

    
    @Override
    public String toString() {
        return nombre;
    }

}
