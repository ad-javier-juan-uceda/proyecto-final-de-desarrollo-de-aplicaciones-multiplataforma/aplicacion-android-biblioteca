package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

public class Usuario implements Serializable {

    private String nombre;
    private String DNI;
    private Date fecha_de_nacimiento;
    private String direccion;
    private String localidad;
    private int id;
    private String contrasenya;

    public Usuario(String nombre, String DNI, Date fecha_de_nacimiento, String direccion, String localidad, String contrasenya) {
        this.nombre = nombre;
        this.DNI = DNI;
        this.fecha_de_nacimiento = fecha_de_nacimiento;
        this.direccion = direccion;
        this.localidad = localidad;
        this.contrasenya = DAO.desencriptar_contrasenya(contrasenya);
    }
    public Usuario() {
        this.nombre = Constantes.VACIO;
        this.DNI = Constantes.VACIO;
        this.fecha_de_nacimiento = new Date();
        this.direccion = Constantes.VACIO;
        this.localidad = Constantes.VACIO;
        id = 0;
        this.contrasenya = DAO.desencriptar_contrasenya("1234");
    }

    public Usuario(Usuario socio) {
        this.nombre = socio.getNombre();
        this.DNI = socio.getDNI();
        this.fecha_de_nacimiento = socio.getFecha_de_nacimiento();
        this.direccion = socio.getDireccion();
        this.localidad = socio.getLocalidad();
        id = socio.getId();
        this.contrasenya = socio.getContrasenya();
    }

    public String getContrasenya() {
        return contrasenya;
    }

    public void setContrasenya(String contrasenya) {
        this.contrasenya = contrasenya;
        //this.contrasenya = DAOMetodos.desencriptar_contrasenya(this.contrasenya);
    }
    
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDNI() {
        return DNI;
    }

    public void setDNI(String DNI) {
        this.DNI = DNI;
    }

    public Date getFecha_de_nacimiento() {
        return fecha_de_nacimiento;
    }

    public void setFecha_de_nacimiento(Date fecha_de_nacimiento) {
        this.fecha_de_nacimiento = fecha_de_nacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Usuario other = (Usuario) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        if (!Objects.equals(this.DNI, other.DNI)) {
            return false;
        }
        if (!Objects.equals(this.direccion, other.direccion)) {
            return false;
        }
        if (!Objects.equals(this.localidad, other.localidad)) {
            return false;
        }
        return Objects.equals(this.fecha_de_nacimiento, other.fecha_de_nacimiento);
    }

    public boolean Equals(Usuario objeto) {
        boolean igual = true;
        try {
            if (!(
                    objeto.getDNI().equals(this.getDNI())
                            && objeto.getDireccion().equals(this.getDireccion())
                            && objeto.getLocalidad().equals(this.getLocalidad())
                            && objeto.getNombre().equals(this.getNombre())
                            && objeto.getFecha_de_nacimiento().equals(this.getFecha_de_nacimiento())
            )) {
                throw new Exception();
            }

        } catch (Exception e) {
            igual = false;
        }
        return igual;
    }

    @Override
    public String toString() {
        return nombre;
    }
}
