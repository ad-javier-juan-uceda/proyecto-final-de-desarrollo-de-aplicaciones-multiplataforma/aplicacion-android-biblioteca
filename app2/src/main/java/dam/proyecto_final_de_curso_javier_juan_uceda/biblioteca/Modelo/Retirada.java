package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Date;
import java.util.Objects;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;

public class Retirada {

    private Date fecha_que_se_retira_el_carnet;
    private Date fecha_que_se_devuelve_el_carnet;
    private String estado_del_libro;
    private Socio socio;
    private int id;

    public Retirada(Date fecha_que_se_retira_el_carnet, Date fecha_que_se_devuelve_el_carnet, String estado_del_libro, Socio socio) {
        this.fecha_que_se_retira_el_carnet = fecha_que_se_retira_el_carnet;
        this.fecha_que_se_devuelve_el_carnet = fecha_que_se_devuelve_el_carnet;
        this.estado_del_libro = estado_del_libro;
        this.socio = socio;
    }

    public Retirada() {
        this.fecha_que_se_retira_el_carnet = new Date();
        this.fecha_que_se_devuelve_el_carnet = new Date();
        this.estado_del_libro = Constantes.VACIO;
        this.socio = new Socio();
        id = 0;
    }

    public Retirada(Retirada retirada) {
        this.fecha_que_se_retira_el_carnet = retirada.getFecha_que_se_retira_el_carnet();
        this.fecha_que_se_devuelve_el_carnet = retirada.getFecha_que_se_devuelve_el_carnet();
        this.estado_del_libro = retirada.getEstado_del_libro();
        this.socio = retirada.getSocio();
        id = retirada.getId();
    }

    public Date getFecha_que_se_retira_el_carnet() {
        return fecha_que_se_retira_el_carnet;
    }

    public void setFecha_que_se_retira_el_carnet(Date fecha_que_se_retira_el_carnet) {
        this.fecha_que_se_retira_el_carnet = fecha_que_se_retira_el_carnet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFecha_que_se_devuelve_el_carnet() {
        return fecha_que_se_devuelve_el_carnet;
    }

    public void setFecha_que_se_devuelve_el_carnet(Date fecha_que_se_devuelve_el_carnet) {
        this.fecha_que_se_devuelve_el_carnet = fecha_que_se_devuelve_el_carnet;
    }

    public String getEstado_del_libro() {
        return estado_del_libro;
    }

    public void setEstado_del_libro(String estado_del_libro) {
        this.estado_del_libro = estado_del_libro;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Retirada other = (Retirada) obj;
        if (!Objects.equals(this.estado_del_libro, other.estado_del_libro)) {
            return false;
        }
        if (!Objects.equals(this.fecha_que_se_retira_el_carnet, other.fecha_que_se_retira_el_carnet)) {
            return false;
        }
        if (!Objects.equals(this.fecha_que_se_devuelve_el_carnet, other.fecha_que_se_devuelve_el_carnet)) {
            return false;
        }
        return Objects.equals(this.socio, other.socio);
    }

    public boolean Equals(Retirada objeto) {
        boolean igual = true;
        try {
            if (!(
                    objeto.getEstado_del_libro().equals(this.getEstado_del_libro())
                            && objeto.getFecha_que_se_devuelve_el_carnet().equals(this.getFecha_que_se_devuelve_el_carnet())
                            && objeto.getEstado_del_libro().equals(this.getEstado_del_libro())
                            && objeto.getSocio().equals(this.getSocio())
            )) {
                throw new Exception();
            }
        } catch (Exception e) {
            igual = false;
        }
        return igual;
    }

    @Override
    public String toString() {
        return socio.toString();
    }

}
