package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads.ThreadLibro;

public class crear_prestamo extends AppCompatActivity implements AdapterView.OnItemSelectedListener, MenuItem.OnMenuItemClickListener {

    private Libro libro;
    private EditText edTitulo;
    private TextView tvTitulo;
    private EditText edISBN;
    private TextView tvISBN;
    private EditText edCantidad;
    private EditText edPaginas;
    private Switch swPrestado;
    private Spinner spinnerAutor;
    private Spinner spinnerEditorial;
    private Spinner spinnerLocalizacion;
    private Spinner spinnerTipo;
    private Spinner spinnerEstado;
    private Button btActualizar;
    private TextView tvCantidad;
    private TextView tvpaginas;
    private TextView tvPrestado;
    private TextView tvAutor;
    private TextView tvEditorial;
    private TextView tvLocalizacion;
    private TextView tvTipo;
    private TextView tvEstado;
    private boolean insertar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_prestamo);
        setUI();
    }

    private void setUI() {

        int libroID = getIntent().getIntExtra("Libro", 0);

        if (libroID != 0) {
            try {
                Thread thread2 = new Thread() {
                    public void run() {
                        try {

                            if (isNetworkAvailable()) {
                                libro = new Libro(new LibroDAO().buscarPorClavePrimaria(libroID));
                            } else {
                                crear_toast(getString(R.string.no_hay_conexion), false);
                            }
                            interrupt();
                        } catch (Exception e) {

                        }
                    }
                };

                thread2.start();

                while (true) if (thread2.isInterrupted()) break;
                insertar = false;
            } catch (Exception e) {
                insertar = true;
            }

        } else {
            insertar = true;
        }


        edTitulo = findViewById(R.id.edTitulo);
        tvTitulo = findViewById(R.id.tvTitulo);
        edISBN = findViewById(R.id.edISBN);
        tvISBN = findViewById(R.id.tvISBN);
        edCantidad = findViewById(R.id.edCantidad);
        edPaginas = findViewById(R.id.edPaginas);
        swPrestado = findViewById(R.id.swPrestado);
        spinnerAutor = findViewById(R.id.spinnerAutor);
        spinnerEditorial = findViewById(R.id.spinnerEditorial);
        spinnerLocalizacion = findViewById(R.id.spinnerLocalizacion);
        spinnerTipo = findViewById(R.id.spinnerTipo);
        spinnerEstado = findViewById(R.id.spinnerEstado);
        btActualizar = findViewById(R.id.btActualizar);
        tvCantidad = findViewById(R.id.tvCantidad);
        tvpaginas = findViewById(R.id.tvpaginas);
        tvPrestado = findViewById(R.id.tvPrestado);
        tvAutor = findViewById(R.id.tvAutor);
        tvEditorial = findViewById(R.id.tvEditorial);
        tvLocalizacion = findViewById(R.id.tvLocalizacion);
        tvTipo = findViewById(R.id.tvTipo);
        tvEstado = findViewById(R.id.tvEstado);

        if (insertar) {
            libro = new Libro();
            setTitle(getString(R.string.insertar_libro));
            btActualizar.setText(getString(R.string.insertar_libro));
        } else {
            setTitle(getString(R.string.editar_libro));
            btActualizar.setText(getString(R.string.editar_libro));
        }


        btActualizar.setOnClickListener(v -> prestar_libro());
        try {

            ArrayAdapter<CharSequence> spinnerAdapterprority = ArrayAdapter.createFromResource(this, R.array.estados, android.R.layout.simple_spinner_item);
            spinnerAdapterprority.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinnerEstado.setAdapter(spinnerAdapterprority);
            spinnerEstado.setOnItemSelectedListener(this);


            actualizar(true);


        } catch (Exception e) {

        }


    }

    private void actualizar(boolean coger) throws Exception {

        if (coger) {

            swPrestado.setChecked(false);

            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, ThreadLibro.autores);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerAutor.setAdapter(adapter);
            spinnerAutor.setOnItemSelectedListener(this);


            adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, ThreadLibro.editoriales);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerEditorial.setAdapter(adapter);
            spinnerEditorial.setOnItemSelectedListener(this);

            adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, ThreadLibro.localizaciones);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerLocalizacion.setAdapter(adapter);
            spinnerLocalizacion.setOnItemSelectedListener(this);

            adapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, ThreadLibro.tipos);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinnerTipo.setAdapter(adapter);
            spinnerTipo.setOnItemSelectedListener(this);

            if (!insertar) {
                edTitulo.setText(libro.getTitulo());
                edISBN.setText(libro.getISBN());
                edCantidad.setText(String.valueOf(libro.getCantidad()));
                edPaginas.setText(String.valueOf(libro.getPaginas()));
                swPrestado.setChecked(libro.isEsta_prestado());
                spinnerAutor.setSelection(ThreadLibro.encontrar(libro.getAutor()));
                spinnerEditorial.setSelection(ThreadLibro.encontrar(libro.getEditorial()));
                spinnerLocalizacion.setSelection(ThreadLibro.encontrar(libro.getLocalizacion()));
                spinnerTipo.setSelection(ThreadLibro.encontrar(libro.getTipo_del_libro()));
                spinnerEstado.setSelection(Integer.parseInt(libro.getEstado_del_libro()));
            }

        } else {

            libro.setTitulo(edTitulo.getText().toString());
            libro.setISBN(edISBN.getText().toString());
            libro.setCantidad(Integer.parseInt(edCantidad.getText().toString()));
            libro.setPaginas(Integer.parseInt(edPaginas.getText().toString()));
            libro.setEsta_prestado(swPrestado.isChecked());

        }

    }


    public void prestar_libro() {

        Thread thread = new Thread() {
            public void run() {

                try {

                    if (insertar) {
                        insertar();
                    } else {
                        actualizar(false);
                        new LibroDAO().actualizarRegistro(libro);
                        crear_toast(getString(R.string.actualizacionCompletada), true);
                    }
                } catch (Exception e) {
                    crear_toast(getString(R.string.actualizacionNoCompletada), false);
                }

            }
        };
        thread.start();
    }

    public void insertar() {

        Thread thread = new Thread() {
            public void run() {

                try {
                    if (isNetworkAvailable()) {
                        actualizar(false);
                        new LibroDAO().insertarRegistro(libro);
                        ThreadLibro threadLibro = new ThreadLibro();
                        threadLibro.start();
                        while (true) if (threadLibro.isInterrupted()) break;
                        crear_toast(getString(R.string.insercionCompletada), true);
                    } else {
                        crear_toast(getString(R.string.no_hay_conexion), false);
                    }

                } catch (Exception e) {
                    crear_toast(getString(R.string.insercionNoCompletada), false);
                }

            }
        };
        thread.start();
    }

    public void crear_toast(final String text_to_print, final boolean acierto) {

        runOnUiThread(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate((acierto ? R.layout.toast_personalizado_acierto : R.layout.toast_personalizado_error), findViewById(R.id.toast_personalizado));

            TextView text = layout.findViewById(R.id.textView);
            text.setText(text_to_print);

            android.widget.Toast toast = new android.widget.Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(android.widget.Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        });


    }

    public boolean isNetworkAvailable() {
        boolean networkAvailable = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();
            networkAvailable = (networkInfo != null && networkInfo.isConnected());
        }

        return networkAvailable;

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinnerAutor:
                libro.setAutor(ThreadLibro.autores.get(position));
                break;
            case R.id.spinnerEditorial:
                libro.setEditorial(ThreadLibro.editoriales.get(position));
                break;
            case R.id.spinnerLocalizacion:
                libro.setLocalizacion(ThreadLibro.localizaciones.get(position));
                break;
            case R.id.spinnerTipo:
                libro.setTipo_del_libro(ThreadLibro.tipos.get(position));
                break;
            case R.id.spinnerEstado:
                libro.setEstado_del_libro(String.valueOf(position));
                break;
            default:
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
