package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView.Adaptador;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads.ThreadLibro;

import static dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.MainActivity.escribir_en_preferencias;

public class recyclerViewLibros extends AppCompatActivity implements Adaptador.OnItemClickListener,
        View.OnClickListener, MenuItem.OnMenuItemClickListener {

    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private FloatingActionButton boton_flotante;
    private int pos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view_libros);

        Context context = getApplicationContext();
        recyclerView = findViewById(R.id.recyclerView);
        boton_flotante = findViewById(R.id.boton_flotante);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        if (ThreadLibro.libros != null) {
            dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView.Adaptador adaptador = new Adaptador(context, ThreadLibro.libros, ((activityName) -> Click(activityName)));
            recyclerView.setAdapter(adaptador);

        } else {
            Thread thread2 = new Thread() {
                public void run() {
                    try {
                        if (isNetworkAvailable()) {
                            ThreadLibro.libros = new LibroDAO().buscarTodos();
                        } else {
                            crear_toast(getString(R.string.no_hay_conexion), false);
                        }

                        interrupt();
                    } catch (Exception e) {

                    }
                }
            };

            thread2.start();

            while (true) if (thread2.isInterrupted()) break;

            dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView.Adaptador adaptador = new Adaptador(context, ThreadLibro.libros, ((activityName) -> Click(activityName)));
            recyclerView.setAdapter(adaptador);

        }

        try {
            enableSwipe();
            refreshLayout = findViewById(R.id.refresh_layout);

            refreshLayout.setOnRefreshListener(
                    () -> {

                        actualizar();
                    }
            );

            refreshLayout.setColorSchemeResources(
                    R.color.color1azul,
                    R.color.color2amarillo,
                    R.color.color3verde,
                    R.color.color4naranja,
                    R.color.color5negro,
                    R.color.color6violeta,
                    R.color.color7marron,
                    R.color.color8rosa,
                    R.color.color9fucsia,
                    R.color.color10rojo
            );

            boton_flotante.setOnClickListener(v -> Insertar());

        } catch (Exception e) {

        }

    }


    private void filtrar(String newText) {
        ArrayList<Libro> lista_libros = new ArrayList();
        for (Libro libro : ThreadLibro.libros) {
            if (libro.getTitulo().toLowerCase().contains(newText)) {
                lista_libros.add(libro);
            }
        }
        dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView.Adaptador adaptador1 = new Adaptador(getApplicationContext(), lista_libros, ((activityName) -> Click(activityName)));
        recyclerView.setAdapter(adaptador1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navegador, menu);

        MenuItem cerrar_sesion = menu.findItem(R.id.action_settings);
        cerrar_sesion.setOnMenuItemClickListener(this);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    filtrar(newText);
                    return true;
                }
            });
        }

        return true;
    }

    public boolean isNetworkAvailable() {
        boolean networkAvailable = false;
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm != null) {
            NetworkInfo networkInfo = cm.getActiveNetworkInfo();

            if (networkInfo != null && networkInfo.isConnected()) {
                networkAvailable = true;
            }
        }

        return networkAvailable;

    }

    private void actualizar() {
        Thread thread = new Thread() {
            public void run() {
                refreshLayout.setRefreshing(true);
                ThreadLibro.libros.clear();
                try {
                    if (isNetworkAvailable()) {
                        ThreadLibro.libros.addAll(new LibroDAO().buscarTodos());
                    } else {
                        crear_toast(getString(R.string.no_hay_conexion), false);
                    }

                } catch (Exception e) {

                }

                if (refreshLayout.isRefreshing()) {
                    refreshLayout.setRefreshing(false);
                }

                try {
                    runOnUiThread(() -> {
                        Adaptador adaptador1 = new Adaptador(getApplicationContext(), ThreadLibro.libros, ((activityName) -> Click(activityName)));
                        recyclerView.setAdapter(adaptador1);
                    });
                } catch (NullPointerException n) {

                }
                interrupt();
            }
        };
        thread.start();
    }

    private AlertDialog crearDialogoConexion(String titulo, String mensaje, final Libro posicion) {
        // Instanciamos un nuevo AlertDialog Builder y le asociamos titulo y mensaje
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(titulo);
        alertDialogBuilder.setMessage(mensaje);

        // Creamos un nuevo OnClickListener para el boton OK que realice la conexion
        DialogInterface.OnClickListener listenerOk = (dialog, which) -> {

            Thread thread = new Thread() {
                public void run() {

                    try {
                        new LibroDAO().eliminarRegistro(posicion);
                        ThreadLibro threadLibro = new ThreadLibro();
                        threadLibro.start();
                        while (!threadLibro.isInterrupted()) {

                        }
                        runOnUiThread(() -> {
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                            Adaptador adaptador = new Adaptador(getApplicationContext(), ThreadLibro.libros, ((activityName) -> Click(activityName)));
                            recyclerView.setAdapter(adaptador);
                            crear_toast(getString(R.string.eliminacionCompletada), true);
                        });


                    } catch (Exception e) {
                        crear_toast(getString(R.string.eliminacionNoCompletada) + e.toString() + e.getMessage(), false);
                    }

                }
            };
            thread.start();

        };

        // Creamos un nuevo OnClickListener para el boton Cancelar
        DialogInterface.OnClickListener listenerCancelar = (dialog, which) -> {
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            Adaptador adaptador = new Adaptador(getApplicationContext(), ThreadLibro.libros, ((activityName) -> Click(activityName)));
            recyclerView.setAdapter(adaptador);
            recyclerView.scrollToPosition(ThreadLibro.encontrar(posicion));
        };

        // Asignamos los botones positivo y negativo a sus respectivos listeners
        alertDialogBuilder.setPositiveButton(getString(R.string.Confirmacion), listenerOk);
        alertDialogBuilder.setNegativeButton(getString(R.string.Negacion), listenerCancelar);

        return alertDialogBuilder.create();
    }

    public void crear_toast(final String text_to_print, final boolean acierto) {

        runOnUiThread(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate((acierto ? R.layout.toast_personalizado_acierto : R.layout.toast_personalizado_error), findViewById(R.id.toast_personalizado));

            TextView text = layout.findViewById(R.id.textView);
            text.setText(text_to_print);

            android.widget.Toast toast = new android.widget.Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(android.widget.Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        });


    }

    private void enableSwipe() {

        //TODO Ex3: enableSwipe

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int position = viewHolder.getAdapterPosition();

                if (direction == ItemTouchHelper.RIGHT) {
                    if (isNetworkAvailable()) {
                        crearDialogoConexion(
                                getString(R.string.EliminacionLibroPregunta)
                                        + ThreadLibro.libros.get(position).getTitulo(),
                                getString(R.string.Pregunta) + getString(R.string.EliminacionLibroPregunta)
                                        + ThreadLibro.libros.get(position).getTitulo(),
                                ThreadLibro.libros.get(position)).show();
                    } else {
                        crear_toast(getString(R.string.no_hay_conexion), false);
                    }

                }
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }


    @Override
    public boolean onMenuItemClick(MenuItem item) {
        escribir_en_preferencias(0);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        return false;
    }

    @Override
    public void onClick(View v) {

    }


    @Override
    public void Click(Libro activityName) {
        Intent intent = new Intent(getApplicationContext(), crear_prestamo.class);
        intent.putExtra("Libro", activityName.getId());
        startActivity(intent);
    }

    public void Insertar() {
        Intent intent = new Intent(getApplicationContext(), crear_prestamo.class);
        startActivity(intent);
    }
}
