package org.GenericoDAO;

import java.util.ArrayList;
import java.util.List;

/**
 * @param <Tipo>
 */
public interface GenericoDAO<Tipo> {

    /*
        @author Javier Juan Uceda
        <b>buscartodos</b>
        Este método busca todos los resgistros.
        @return todos los resgistros.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */

    ArrayList<Tipo> buscarTodos() throws Exception;

    /*
        @author Javier Juan Uceda
        <b>buscarPorClavePrimaria</b>
        Este método busca el registro que coincida con su clave primaria
        por el pasado por parámetro.
        @param id que es la clave primaria pasada como parámetro.
        @return El registro.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    Tipo buscarPorClavePrimaria(int id) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>buscarPorTodasLasClavePrimaria</b>
        Este método busca todos los registros que coincidan con las llaves primarias.
        por el pasado por parámetro.
        @param id que es la clave primaria pasada como parámetro.
        @return todos los resgistros.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    ArrayList<Tipo> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>insertarRegistro</b>
        Este método inserta un registro que es el objeto pasado como parámetro.
        por el pasado por parámetro.
        @param objeto que es el objeto pasado como parámetro.
        @return la id del objeto.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    Integer insertarRegistro(Tipo objeto) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>actualizarRegistro</b>
        Este método actualiza un registro que es el objeto pasado como parámetro.
        por el pasado por parámetro.
        @param objeto que es el objeto pasado como parámetro.
        @return true o Constantes.BOOLEAN_POR_DEFECTO si ha actualizado bien.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    boolean actualizarRegistro(Tipo objeto) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>eliminarRegistro</b>
        Este método elimina un registro que es el objeto pasado como parámetro.
        por el pasado por parámetro.
        @param objeto que es el objeto pasado como parámetro.
        @return true o Constantes.BOOLEAN_POR_DEFECTO si ha eliminado bien.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    boolean eliminarRegistro(Tipo objeto) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>eliminarRegistro</b>
        Este método elimina un registro que es el objeto pasado como parámetro.
        por el pasado por parámetro.
        @param id del objeto pasado como parámetro.
        @return true o Constantes.BOOLEAN_POR_DEFECTO si ha eliminado bien.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    boolean eliminarRegistro(int idObjeto) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>obtenerLista</b>
        Este método obtiene los registros de la lista sacada por el API de odoo.
        @param objetos pasado como parámetro sacada por el API de odoo.
        @return La lista de objetos.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    ArrayList<Tipo> obtenerLista(List<Object> listaObjetos) throws Exception;

    /*
        @author Javier Juan Uceda
        <b>insertarMasDeUnRegistro</b>
        Este método inserta los registros que son los objetos pasados como parámetro.
        @param Los objetos pasados como parámetro.
        @return las ids de los objetos.
        @throws MalformedURLException
        @throws XmlRpcException
        @throws Exception
     */
    Integer[] insertarMasDeUnRegistro(ArrayList<Tipo> listaObjetos) throws Exception;
}
