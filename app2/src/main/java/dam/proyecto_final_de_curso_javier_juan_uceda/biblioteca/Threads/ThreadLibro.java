package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads;

import java.util.ArrayList;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.AutorDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.EditorialDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LocalizacionDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.TipoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Autor;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Editorial;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Localizacion;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Tipo;

public class ThreadLibro extends Thread {
    public static ArrayList<Libro> libros;
    public static ArrayList<Autor> autores;
    public static ArrayList<Editorial> editoriales;
    public static ArrayList<Localizacion> localizaciones;
    public static ArrayList<Tipo> tipos;

    public ThreadLibro() {
        libros = new ArrayList<>();
        autores = new ArrayList<>();
        editoriales = new ArrayList<>();
        localizaciones = new ArrayList<>();
        tipos = new ArrayList<>();
    }

    public static int encontrar(Editorial editorial) {
        int encontrado = -1;
        for (int contador = 0; contador < editoriales.size(); contador++) {
            if (editoriales.get(contador).getId() == editorial.getId()) {
                contador = editoriales.size();
            }
        }
        return encontrado;
    }

    public static int encontrar(Autor autor) {
        int encontrado = -1;
        for (int contador = 0; contador < autores.size(); contador++) {
            if (autores.get(contador).getId() == autor.getId()) {
                contador = autores.size();
            }
        }
        return encontrado;
    }

    public static int encontrar(Tipo tipo) {
        int encontrado = -1;
        for (int contador = 0; contador < tipos.size(); contador++) {
            if (tipos.get(contador).getId() == tipo.getId()) {
                contador = tipos.size();
            }
        }
        return encontrado;
    }

    public static int encontrar(Localizacion localizacion) {
        int encontrado = -1;
        for (int contador = 0; contador < localizaciones.size(); contador++) {
            if (localizaciones.get(contador).getId() == localizacion.getId()) {
                contador = localizaciones.size();
            }
        }
        return encontrado;
    }

    public static int encontrar(Libro libro) {
        int encontrado = -1;
        for (int contador = 0; contador < libros.size(); contador++) {
            if (libros.get(contador).getId() == libro.getId()) {
                contador = libros.size();
            }
        }
        return encontrado;
    }

    @Override
    public void run() {
        super.run();

        try {
            libros.clear();
            autores.clear();
            editoriales.clear();
            localizaciones.clear();
            tipos.clear();
            libros.addAll(new LibroDAO().buscarTodos());
            autores.addAll(new AutorDAO().buscarTodos());
            editoriales.addAll(new EditorialDAO().buscarTodos());
            localizaciones.addAll(new LocalizacionDAO().buscarTodos());
            tipos.addAll(new TipoDAO().buscarTodos());
            interrupt();
        } catch (Exception e) {

        }
    }
}
