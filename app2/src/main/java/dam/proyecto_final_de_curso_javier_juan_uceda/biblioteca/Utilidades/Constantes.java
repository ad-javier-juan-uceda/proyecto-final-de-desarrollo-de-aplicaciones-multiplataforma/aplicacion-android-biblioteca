package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.File;

/**
 * @author Javier Juan Uceda
 */
public class Constantes {

    /**
     *
     */
    public static final int TOAST_DELAY = 3500;
    /**
     *
     */
    public static final int FADE_IN_DELAY0 = 500;
    /**
     *
     */
    public static final int FADE_OUT_DELAY = 500;
    /**
     *
     */
    public static final String ARCHIVO_PREFERENCIAS = "preferencias";
    /**
     *
     */
    public static final String MODO_DIA = "modo_dia";
    /**
     *
     */
    public static final String MODO_NOCHE = "modo_noche";
    /**
     *
     */
    public static final String IDIOMA_POR_DEFECTO = "espanyol";
    /**
     *
     */
    public static final String INGLES = "ingles";
    /**
     *
     */
    public static final String FRANCES = "frances";
    /**
     *
     */
    public static final String ALEMAN = "aleman";
    /**
     *
     */
    public static final String ITALIANO = "italiano";
    /**
     *
     */
    public static final boolean BOOLEAN_POR_DEFECTO = false;
    /**
     *
     */
    public static final String IDIOMA = "idioma";
    /**
     *
     */
    public static final String MODO_IMAGEN = "modo_imagen";
    /**
     *
     */
    public static final String COLOR_POR_DEFECTO = "0x000000";
    /**
     *
     */
    public static final String COLOR = "color";
    /**
     *
     */
    public static final String EXPORTAR_AYUDA_TOOLTIP = "exporta_esta_pantalla";
    /**
     *
     */
    public static final String IMAGEN_DE_LA_APLICACION = "imagen_de_la_aplicacion";
    /**
     *
     */
    public static final String IMAGEN_DE_FONDO = "imagen_de_fondo";
    /**
     *
     */
    public static final String USUARIO_RECORDADO = "usuario_recordado";
    /**
     *
     */
    public static final String INICIAR_SESION = "iniciar_sesion";
    /**
     *
     */
    public static final String CERRAR_SESION = "cerrar_sesion";
    /**
     *
     */
    public static final String SALIR_DE_LA_PANTALLA_COMPLETA = "salirdepantallacompleta";
    /**
     *
     */
    public static final String PANTALLA_COMPLETA = "pantallacompleta";
    /**
     *
     */
    public static final String PONER_EN_PANTALLA_COMPLETA = "ponerenpantallacompleta";
    /**
     *
     */
    public static final String MODO_FONDO_IMAGEN = "modo_fondo_imagen";
    /**
     *
     */
    public static final String MODO_FONDO_COLOR = "modo_fondo_color";
    /**
     *
     */
    public static final String ARCHIVOS = "Archivos";
    /**
     *
     */
    public static final String JPG = "JPG";
    /**
     *
     */
    public static final String PNG = "PNG";
    /**
     *
     */
    public static final String JPEG = "JPEG";
    /**
     *
     */
    public static final String ELIGE_UNA_IMAGEN = "elige_una_imagen";
    /**
     *
     */
    public static final String ELIGE_PREFERENCIAS = "elige_preferencias";
    /**
     *
     */
    public static final String MAXIMIZADO = "maximizado";
    /**
     *
     */
    public static final String TITULO_APLICACION = "Titulo_aplicacion";
    /**
     *
     */
    public static final String TITULO_APLICACION_POR_DEFECTO = "Gestor de biblioteca";
    /**
     *
     */
    public static final String PREFERENCIAS_EXITO = "preferencias_exito";
    /**
     *
     */
    public static final String PREFERENCIAS_NO_EXITO = "preferencias_no_exito";
    /**
     *
     */
    public static final String PREFERENCIAS_GUARDADAS_CON_EXITO = "preferencias_guardadas_exito";
    /**
     *
     */
    public static final String GUARDAR_PREFERENCIAS = "guardar_preferencias";
    /**
     *
     */
    public static final String SEPARADOR_EXPORTACION = ";";
    /**
     *
     */
    public static final String INSERCION_AUTOR_CORRECTA = "insertado_autor_correcto";
    /**
     *
     */
    public static final String INSERCION_AUTOR_NO_CORRECTA = "insertado_autor_no_correcto";
    /**
     *
     */
    public static final String ACTUALIZACION_AUTOR_CORRECTA = "actualizacion_autor_correcto";
    /**
     *
     */
    public static final String ACTUALIZACION_AUTOR_NO_CORRECTA = "actualizacion_autor_no_correcto";
    /**
     *
     */
    public static final String CONFIRMACION_EXPORTACION = "confirmacion_exportacion_autor";
    /**
     *
     */
    public static final String FACEBOOK = "facebook";
    /**
     *
     */
    public static final String PAGINA_WEB = "pagina_web";
    /**
     *
     */
    public static final String TWITTER = "twitter";
    /**
     *
     */
    public static final String INSTAGRAM = "instagram";
    /**
     *
     */
    public static final String ELIMINAR = "Eliminar";
    /**
     *
     */
    public static final String ELIMINAR_TODOS_LOS_SELECCIONADOS = "Eliminar_todos_los_elementos";
    /**
     *
     */
    public static final String PREGUNTA_ELIMINAR = "pregunta_eliminar";
    /**
     *
     */
    public static final String EXPORTAR_FILE_CHOOSER = "exportar_autor";
    /**
     *
     */
    public static final String XML = "XML";
    /**
     *
     */
    public static final String XML_TIPO = "*.xml";
    /**
     *
     */
    public static final String CSV = ".csv";
    /**
     *
     */
    public static final String JPG_TIPO = "*.jpeg";
    /**
     *
     */
    public static final String JPEG_TIPO = "*.jpg";
    /**
     *
     */
    public static final String PNG_TIPO = "*.png";
    /**
     *
     */
    public static final String VACIO = "";
    /**
     *
     */
    public static final String TITULO_LIBRO = "Titulo";
    /**
     *
     */
    public static final String ISBN = "ISBN";
    /**
     *
     */
    public static final String BUEN_ESTADO = "Buen_estado";
    /**
     *
     */
    public static final String ESTROPEADO = "Estropeado";
    /**
     *
     */
    public static final String PAGINAS_ROTAS = "paginas_rotas";
    /**
     *
     */
    public static final String NO_PRESTADO = "no_se_ha_podido_prestar";
    /**
     *
     */
    public static final String INUTILIZABLE = "Inutilizable";
    /**
     *
     */
    public static final String NOMBRE_EDITORIAL = "nombre";
    /**
     *
     */
    public static final String TELEFONO_EDITORIAL = "telefono";
    /**
     *
     */
    public static final String DIRECCION = "direccion";
    /**
     *
     */
    public static final String LOCALIDAD = "localidad";
    /**
     *
     */
    public static final String CIF = "CIF";
    /**
     *
     */
    public static final String ANYADIR_UN_LIBRO = "Anyadir_libro";
    /**
     *
     */
    public static final String NOMBRE_TIPO = "nombre";
    /**
     *
     */
    public static final String DESCRIPCION_TIPO = "descripcion";
    /**
     *
     */
    public static final String ESPACIO = " ";
    /**
     *
     */
    public static final String RETORNON = "\n";
    /**
     *
     */
    public static final String LOGUEADO_CORRECTO = "logueado_correcto";
    /**
     *
     */
    public static final String LOGUEADO_INCORRECTO = "logueado_incorrecto";
    /**
     *
     */
    public static final String MOSTRAR_CONTRASENYA = "mostrar_contrasenya";
    /**
     *
     */
    public static final String OCULTAR_CONTRASENYA = "ocultar_contrasenya";
    /**
     *
     */
    public static final File CARPETA_PERSONAL = new File(System.getProperty("user.home"));
}
