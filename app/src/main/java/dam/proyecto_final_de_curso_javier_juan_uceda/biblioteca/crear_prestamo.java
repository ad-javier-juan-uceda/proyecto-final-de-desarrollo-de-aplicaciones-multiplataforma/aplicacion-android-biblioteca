package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca;

import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.PrestamoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Prestamo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.datepickerdialog.datepickerdialog;

public class crear_prestamo extends AppCompatActivity implements
        datepickerdialog.OnDateSetPickerFragmentListener {

    private Button btfechadevolucion;
    private TextView tvcrearregistro;
    private TextView tvfecha;
    private Button btreservarlibro;
    private Date fecha_devolucion;
    private static Libro libro;
    private Prestamo prestamo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_prestamo);
        setUI();
    }

    private void setUI() {

        btfechadevolucion = findViewById(R.id.btfechadevolucion);
        tvfecha = findViewById(R.id.tvfecha);
        btreservarlibro = findViewById(R.id.btreservarlibro);
        Thread thread2 = new Thread() {
            public void run() {
                try {
                    libro = new Libro(new LibroDAO().buscarPorClavePrimaria(getIntent().getIntExtra("Libro", 0)));
                    interrupt();
                } catch (Exception e) {
                    interrupt();
                }
            }
        };

        thread2.start();
        while (true) if (thread2.isInterrupted()) break;
        setTitle((prestamo != null) ? getString(R.string.devolver_libro) : getString(R.string.prestar_libro));
    }

    public void openDatePickerDialog(View button) {

        // set a Bundle with date info and set fragment Args.
        Bundle dateBundle = new Bundle();
        Date dpDate = (prestamo == null) ? new Date() : prestamo.getFecha_que_se_tiene_que_devolver();
        dateBundle.putInt("year", (prestamo == null) ? dpDate.getYear() + 1900 : dpDate.getYear());
        dateBundle.putInt("month", dpDate.getMonth());
        dateBundle.putInt("day", dpDate.getDate());

        // send data to and get DatePickerFragment instance
        datepickerdialog dateFragment = datepickerdialog.newInstance(dateBundle);

        // Display dateFragment into the activity fragment manager
        dateFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public void prestar_libro(View view) {

        Thread thread = new Thread() {
            public void run() {

                try {

                    if (libro != null) {
                        if (new PrestamoDAO().noHayPrestamo(libro)) {
                            Prestamo prestamo = new Prestamo();
                            prestamo.setFecha_de_prestamo(new Date());
                            prestamo.setFecha_que_se_ha_devuelto(new Date());
                            prestamo.setFecha_que_se_tiene_que_devolver(fecha_devolucion);
                            prestamo.setLibro_prestado(libro);
                            prestamo.setSocio(MainActivity.socio_logueado);

                            new PrestamoDAO().insertarRegistro(prestamo);
                            crear_toast("Insertado", true);
                        } else {
                            crear_toast("Ya esta prestado", false);
                        }
                    }

                } catch (Exception e) {
                    crear_toast(e.getMessage() + e.toString(), false);
                }

            }
        };
        thread.start();
    }

    public void crear_toast(final String text_to_print, final boolean acierto) {

        runOnUiThread(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate((acierto ? R.layout.toast_personalizado_acierto : R.layout.toast_personalizado_error), findViewById(R.id.toast_personalizado));

            TextView text = layout.findViewById(R.id.textView);
            text.setText(text_to_print);

            android.widget.Toast toast = new android.widget.Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(android.widget.Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        });


    }

    // TODO 4 --> implement listener callback method to receive date & Time  from DatePickerDialog, TimePickerDialog
    @Override
    public void onDateSet(int year, int month, int day) {

        fecha_devolucion = new Date();
        fecha_devolucion.setDate(day);
        fecha_devolucion.setMonth(month);
        fecha_devolucion.setYear(year - 1900);
        tvfecha.setText(day + " " + month + " " + year);
    }
}
