package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades;/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import android.os.Build;

import androidx.annotation.RequiresApi;

import org.GenericoDAO.GenericoDAO;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.PedidoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.UsuarioDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Linea_pedido;

/**
 * @author batoi
 */
public class DAO {

    private static final PedidoDAO pedidoDAO = new PedidoDAO();

    public static String fecha_correcta(Date fecha) {
        return (fecha.getYear() + 1900) + "-" + (fecha.getMonth() + 1) + "-" + fecha.getDate();

    }

    public static String transformar_texto(Object texto) {
        String paginaweb = Constantes.VACIO;
        try {
            paginaweb = (String) texto;
        } catch (ClassCastException c) {
            paginaweb = Constantes.VACIO;
        }
        return paginaweb;
    }

    public static Date obtener_fecha(Object fecha_a_obtener) {
        Date fecha;
        try {
            fecha = java.sql.Date.valueOf((String) fecha_a_obtener);
        } catch (Exception e) {
            fecha = new Date();
        }
        return fecha;
    }


    public static ArrayList<Integer> relacion_uno_a_muchos(Object map) {
        ArrayList<Integer> values = new ArrayList();
        Object[] objects = (Object[]) map;
        if (objects.length > 0) {
            for (Object object : objects) {
                values.add((Integer) object);
            }
        }
        return values;
    }

    public static Object[] insertar_relacion_uno_a_muchos(ArrayList<Linea_pedido> lineas_pedido) {
        int tamanyo = lineas_pedido.size();
        Object[] objetos = new Object[tamanyo];

        for (int contador = 0; contador < tamanyo; contador++) {
            objetos[contador] = lineas_pedido.get(contador).getId();
        }

        return objetos;
    }

    public static Object[] insertar_relacion_uno_a_muchos_libro(ArrayList<Libro> lineas_pedido) {
        int tamanyo = lineas_pedido.size();
        Object[] objetos = new Object[tamanyo];

        for (int contador = 0; contador < tamanyo; contador++) {
            objetos[contador] = lineas_pedido.get(contador).getId();
            System.out.println(lineas_pedido.get(contador).getId());
        }

        return objetos;
    }


    public static Object Relacion_uno_a_muchos(HashMap map, String field, String classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return ((GenericoDAO) Class.forName(classNameDAO).getDeclaredConstructor().newInstance())
                .buscarPorClavePrimaria(value);
    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, UsuarioDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return classNameDAO.buscarPorClavePrimaria(value);
    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, LibroDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return classNameDAO.buscarPorClavePrimaria(value);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String desencriptar_contrasenya(String texto) {
        String desencripcion = "";
        MessageDigest digest;
        try {
            digest = MessageDigest.getInstance("SHA-1");

            digest.reset();
            digest.update(texto.getBytes(StandardCharsets.UTF_8));
            desencripcion = String.format("%040x", new BigInteger(1, digest.digest()));
        } catch (NoSuchAlgorithmException e) {
            // TODO Bloque catch generado autom�ticamente
            e.printStackTrace();
        }
        return desencripcion;

    }

    public static Object Relacion_uno_a_muchos(HashMap map, String field, PedidoDAO classNameDAO) throws Exception {
        int value = 0;

        Object[] objects = (Object[]) map.get(field);
        if (objects.length > 0) {
            value = (Integer) objects[0];
        }

        return pedidoDAO.buscarPorClavePrimaria(value);
    }


}
