package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads;

import android.content.Context;

import java.util.ArrayList;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;

public class Grupofavoritos {
    private ArrayList<ThreadLibrosfavoritos> lista_threads_libro;

    public Grupofavoritos(ArrayList<Libro> lista_libros, Context contexto) {
        lista_threads_libro = new ArrayList<>();
        for (Libro libro : lista_libros) {
            lista_threads_libro.add(new ThreadLibrosfavoritos(libro, contexto));
        }
        for (ThreadLibrosfavoritos libro : lista_threads_libro) {
            libro.start();
        }
    }

    public ArrayList<ThreadLibrosfavoritos> getLista_threads_libro() {
        return lista_threads_libro;
    }

    public void setLista_threads_libro(ArrayList<ThreadLibrosfavoritos> lista_threads_libro) {
        this.lista_threads_libro = lista_threads_libro;
    }

    public void cancelarthreads() {
        for (ThreadLibrosfavoritos libro : lista_threads_libro) {
            libro.interrupt();
        }
        lista_threads_libro.clear();
    }

    public void reiniciarthreads(ArrayList<Libro> lista_libros, Context contexto) {
        cancelarthreads();
        for (Libro libro : lista_libros) {
            lista_threads_libro.add(new ThreadLibrosfavoritos(libro, contexto));
        }
        for (ThreadLibrosfavoritos libro : lista_threads_libro) {
            libro.start();
        }
    }
}
