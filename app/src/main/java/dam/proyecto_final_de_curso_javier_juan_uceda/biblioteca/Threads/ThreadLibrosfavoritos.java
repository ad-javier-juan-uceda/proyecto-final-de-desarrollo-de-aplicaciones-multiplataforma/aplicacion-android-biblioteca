package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Libro_en_detalle;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.R;

public class ThreadLibrosfavoritos extends Thread {

    private Libro libro_favorito;
    private Context contexto;

    public ThreadLibrosfavoritos(Libro libro_favorito, Context contexto) {
        this.libro_favorito = libro_favorito;
        this.contexto = contexto;
    }

    @Override
    public void run() {
        super.run();

        while (!isInterrupted()) {
            try {
                boolean prestado = libro_favorito.isEsta_prestado();
                libro_favorito = new LibroDAO().buscarPorClavePrimaria(libro_favorito.getId());
                if (libro_favorito.isEsta_prestado() != prestado) {
                    crearnotificacion();
                }
            } catch (Exception e) {
                interrupt();
            }

        }

    }

    private void crearnotificacion() {

        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(contexto, Libro_en_detalle.class);
        intent.putExtra("Libro", libro_favorito);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(contexto, 0, intent, 0);


        NotificationCompat.Builder builder = new NotificationCompat.Builder(contexto, "1")
                .setSmallIcon(R.drawable.ic_menu_camera)
                .setContentTitle("My notification" + libro_favorito.getTitulo())
                .setContentText(libro_favorito.isEsta_prestado() ? " Ha sido prestado" : " Ha sido devuelto")
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText("Much longer text that cannot fit one line..."))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(contexto);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(libro_favorito.getId(), builder.build());
    }
}
