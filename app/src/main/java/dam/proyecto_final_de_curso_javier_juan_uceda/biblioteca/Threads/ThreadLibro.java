package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads;

import java.util.ArrayList;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;

public class ThreadLibro extends Thread {
    public static ArrayList<Libro> libros = new ArrayList<>();

    public ThreadLibro() {

    }

    @Override
    public void run() {
        super.run();

        try {
            if (!isInterrupted()) {
                libros.clear();

                libros.addAll(new LibroDAO().buscarTodos());
                //Log.i("tamanyo","tamanyo array " + libros.size());
                interrupt();
            }
        } catch (Exception e) {

        }
    }
}
