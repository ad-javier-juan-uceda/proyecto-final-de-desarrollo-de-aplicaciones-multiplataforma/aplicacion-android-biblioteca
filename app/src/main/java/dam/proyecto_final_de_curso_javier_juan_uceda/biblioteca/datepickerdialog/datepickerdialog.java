package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.datepickerdialog;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import java.util.Calendar;

public class datepickerdialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    //
    // TODO  4  ---> passing data between a Fragment and Activity using Listener Pattern.
    //

    // TODO  4  --> 2: add a member variable to hold the reference to the listener (will be the activity).
    private OnDateSetPickerFragmentListener listener;

    public datepickerdialog() {
        // empty constructor required to recreate fragment if necessary
    }

    // TODO 4  --> 4: static factory method to create new DatePickerFragment instances so we never forget sending required arguments
    public static datepickerdialog newInstance(Bundle arguments) {

        datepickerdialog fragment = new datepickerdialog();
        // set  fragment args
        if (arguments != null) fragment.setArguments(arguments);

        return fragment;
    }

    // TODO  4  --> 3: when fragment is attached to activity-> set listener  (context is the activity)
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof OnDateSetPickerFragmentListener) {
            listener = (OnDateSetPickerFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement DatePickerFragment.OnDateSetPickerFragmentListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int year;
        int month;
        int day;

        if (getArguments() == null) {
            Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            year = getArguments().getInt("year");
            month = getArguments().getInt("month");
            day = getArguments().getInt("day");
        }

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

        return dialog;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        // bad solution
        //DatePicker dpDate = (DatePicker) getActivity().findViewById(R.id.dpDate);
        //dpDate.updateDate(year, month, day);

        // much better!
        //((EventDataActivity)getActivity()).showChosenDate(year, month, day);

        // much much much better!
        // TODO 4  --> 5: call the listener callback method to set selected year, month and day
        notifyTimePickerListener(year, month, day);

    }

    private void notifyTimePickerListener(int year, int month, int day) {
        if (listener != null)
            listener.onDateSet(year, month, day);
    }

    // TODO 4 --> 1: Define an interface that activity must implement
    public interface OnDateSetPickerFragmentListener {
        void onDateSet(int year, int month, int day);
    }
}