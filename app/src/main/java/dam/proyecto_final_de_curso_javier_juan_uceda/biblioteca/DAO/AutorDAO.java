package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import android.os.Build;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.GenericoDAO.GenericoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Autor;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class AutorDAO implements GenericoDAO<Autor> {

    private final XmlRpcClient Connection;
    private final EditorialDAO editorialDAO;

    private static final String NOMBRE = Constantes.NOMBRE_TIPO;
    private static final String TELEFONO = "telefono";
    private static final String FECHA_DE_NACIMIENTO = "fecha_de_nacimiento";
    private static final String PAGINA_WEB = Constantes.PAGINA_WEB;
    private static final String FACEBOOK = Constantes.FACEBOOK;
    private static final String TWITTER = Constantes.TWITTER;
    private static final String INSTAGRAM = Constantes.INSTAGRAM;
    private static final String NOMBRE_TABLA = "biblioteca.autor";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public AutorDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
        editorialDAO = new EditorialDAO();
    }

    @Override
    public ArrayList<Autor> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, FACEBOOK, INSTAGRAM, TWITTER, PAGINA_WEB, FECHA_DE_NACIMIENTO, TELEFONO));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Autor buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, FACEBOOK, INSTAGRAM, TWITTER, PAGINA_WEB, FECHA_DE_NACIMIENTO, TELEFONO));
                    }
                }
        )));
        ArrayList<Autor> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Autor> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, FACEBOOK,
                                INSTAGRAM, TWITTER, PAGINA_WEB, FECHA_DE_NACIMIENTO, TELEFONO));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Integer insertarRegistro(Autor objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(FACEBOOK, objeto.getFacebook());
                        put(INSTAGRAM, objeto.getInstagram());
                        put(TWITTER, objeto.getTwitter());
                        put(PAGINA_WEB, objeto.getPagina_web());
                        put(FECHA_DE_NACIMIENTO, DAO.fecha_correcta(objeto.getFecha_de_nacimiento()));
                        put(TELEFONO, objeto.getTelefono());
                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Autor objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(NOMBRE, objeto.getNombre());
                                put(FACEBOOK, objeto.getFacebook());
                                put(INSTAGRAM, objeto.getInstagram());
                                put(TWITTER, objeto.getTwitter());
                                put(PAGINA_WEB, objeto.getPagina_web());
                                put(FECHA_DE_NACIMIENTO, DAO.fecha_correcta(objeto.getFecha_de_nacimiento()));
                                put(TELEFONO, objeto.getTelefono());
                            }
                        }
                )
        ));
        return true;

    }

    @Override
    public boolean eliminarRegistro(Autor objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Autor> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Autor> lista = new ArrayList();
        Autor autor;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            autor = new Autor();
            elemento_actual = (HashMap) elemento;

            autor.setId((int) elemento_actual.get(ID));
            autor.setNombre((String) elemento_actual.get(NOMBRE));
            autor.setFacebook(DAO.transformar_texto(elemento_actual.get(FACEBOOK)));
            autor.setInstagram(DAO.transformar_texto(elemento_actual.get(INSTAGRAM)));
            autor.setTwitter(DAO.transformar_texto(elemento_actual.get(TWITTER)));
            autor.setPagina_web(DAO.transformar_texto(elemento_actual.get(PAGINA_WEB)));
            autor.setFecha_de_nacimiento(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_NACIMIENTO)));
            autor.setTelefono((int) elemento_actual.get(TELEFONO));
            lista.add(autor);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Autor> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public Autor buscar(Autor objeto) throws Exception {
        Autor encontrado = null;
        ArrayList<Autor> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (lista.get(i).equals(objeto)) {
                    i = lista.size();
                }
            }
        }
        return encontrado;
    }

}
