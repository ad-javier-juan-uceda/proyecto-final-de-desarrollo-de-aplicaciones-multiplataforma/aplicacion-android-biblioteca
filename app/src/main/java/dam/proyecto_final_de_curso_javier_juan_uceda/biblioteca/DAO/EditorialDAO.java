package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import android.os.Build;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.GenericoDAO.GenericoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Editorial;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;


public class EditorialDAO implements GenericoDAO<Editorial> {

    private XmlRpcClient Connection;
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    private static final String NOMBRE = Constantes.NOMBRE_TIPO;
    private static final String TELEFONO = "telefono";
    private static final String PAGINA_WEB = Constantes.PAGINA_WEB;
    private static final String CIF = "cif";
    private static final String FACEBOOK = Constantes.FACEBOOK;
    private static final String TWITTER = Constantes.TWITTER;
    private static final String INSTAGRAM = Constantes.INSTAGRAM;
    private static final String DIRECCION = "direccion";
    private static final String LOCALIDAD = "localidad";
    private static final String NOMBRE_TABLA = "biblioteca.editorial";

    public EditorialDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
    }

    @Override
    public ArrayList<Editorial> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, CIF, FACEBOOK, INSTAGRAM, TWITTER, PAGINA_WEB, DIRECCION, LOCALIDAD, TELEFONO));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Editorial buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, CIF, FACEBOOK, INSTAGRAM, TWITTER, PAGINA_WEB, DIRECCION, LOCALIDAD, TELEFONO));
                    }
                }
        )));
        ArrayList<Editorial> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Editorial> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE, CIF, FACEBOOK, INSTAGRAM, TWITTER, PAGINA_WEB, DIRECCION, LOCALIDAD, TELEFONO));
                    }
                }
        )));
        ArrayList<Editorial> lista = obtenerLista(list);

        return lista;
    }

    @Override
    public Integer insertarRegistro(Editorial objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());
                        put(FACEBOOK, objeto.getFacebook());
                        put(INSTAGRAM, objeto.getInstagram());
                        put(TWITTER, objeto.getTwitter());
                        put(PAGINA_WEB, objeto.getPagina_web());
                        put(CIF, objeto.getCIF());
                        put(LOCALIDAD, objeto.getLocalidad());
                        put(DIRECCION, objeto.getDireccion());
                        put(TELEFONO, objeto.getTelefono());
                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Editorial objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(
                        asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(NOMBRE, objeto.getNombre());
                                put(FACEBOOK, objeto.getFacebook());
                                put(INSTAGRAM, objeto.getInstagram());
                                put(TWITTER, objeto.getTwitter());
                                put(PAGINA_WEB, objeto.getPagina_web());
                                put(CIF, objeto.getCIF());
                                put(LOCALIDAD, objeto.getLocalidad());
                                put(DIRECCION, objeto.getDireccion());
                                put(TELEFONO, objeto.getTelefono());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Editorial objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Editorial> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Editorial> lista = new ArrayList();
        Editorial editorial;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            editorial = new Editorial();
            elemento_actual = (HashMap) elemento;

            editorial.setId((int) elemento_actual.get(ID));
            editorial.setNombre((String) elemento_actual.get(NOMBRE));
            editorial.setFacebook(DAO.transformar_texto(elemento_actual.get(FACEBOOK)));
            editorial.setInstagram(DAO.transformar_texto(elemento_actual.get(INSTAGRAM)));
            editorial.setTwitter(DAO.transformar_texto(elemento_actual.get(TWITTER)));
            editorial.setPagina_web(DAO.transformar_texto(elemento_actual.get(PAGINA_WEB)));
            editorial.setDireccion(DAO.transformar_texto(elemento_actual.get(DIRECCION)));
            editorial.setLocalidad(DAO.transformar_texto(elemento_actual.get(LOCALIDAD)));
            editorial.setCIF(DAO.transformar_texto(elemento_actual.get(CIF)));
            editorial.setTelefono((int) elemento_actual.get(TELEFONO));
            lista.add(editorial);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Editorial> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public Editorial buscar(Editorial objeto) throws Exception {
        Editorial encontrado = null;
        ArrayList<Editorial> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (lista.get(i).equals(objeto)) {
                    i = lista.size();
                }
            }
        }
        return encontrado;
    }

}