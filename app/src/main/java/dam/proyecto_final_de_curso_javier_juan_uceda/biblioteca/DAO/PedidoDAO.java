package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.GenericoDAO.GenericoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Pedido;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Usuario;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class PedidoDAO implements GenericoDAO<Pedido> {

    private static Linea_pedidoDAO linea_pedidoDAO;
    private static UsuarioDAO usuarioDAO;
    private XmlRpcClient Connection;
    private static final String FECHA_DE_REALIZACION = "fecha_de_realizacion";
    private static final String FECHA_DE_ENTREGA = "fecha_de_entrega";
    private static final String USUARIO = "usuario_id";


    private static final String NOMBRE_TABLA = "biblioteca.pedido";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public PedidoDAO() {
        try {
            Connection = ConexionOdoo.getConnectionOdoo();

            if (linea_pedidoDAO == null) {
                linea_pedidoDAO = new Linea_pedidoDAO();
            }

            if (usuarioDAO == null) {
                usuarioDAO = new UsuarioDAO();
            }

        } catch (MalformedURLException ex) {

        } catch (XmlRpcException ex) {

        } catch (Exception ex) {

        }
    }

    @Override
    public ArrayList<Pedido> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, USUARIO, FECHA_DE_REALIZACION, FECHA_DE_ENTREGA));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Pedido buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, USUARIO, FECHA_DE_REALIZACION, FECHA_DE_ENTREGA));
                    }
                }
        )));
        ArrayList<Pedido> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Pedido> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, USUARIO, FECHA_DE_REALIZACION, FECHA_DE_ENTREGA));
                    }
                }
        )));
        ArrayList<Pedido> lista = obtenerLista(list);

        return lista;
    }

    @Override
    public Integer insertarRegistro(Pedido objeto) throws Exception {
        linea_pedidoDAO.insertarMasDeUnRegistro(objeto.getLineas_pedido());
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(FECHA_DE_REALIZACION, DAO.fecha_correcta(objeto.getFecha_de_realizacion()));
                        put(FECHA_DE_ENTREGA, DAO.fecha_correcta(objeto.getFecha_de_entrega()));
                        put(USUARIO, objeto.getUsuario().getId());
                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Pedido objeto) throws Exception {
        linea_pedidoDAO.actualizarMasDeUnRegistro(objeto.getLineas_pedido());
        Connection.execute(EJECUTAR, asList(ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(FECHA_DE_REALIZACION, DAO.fecha_correcta(objeto.getFecha_de_realizacion()));
                                put(FECHA_DE_ENTREGA, DAO.fecha_correcta(objeto.getFecha_de_entrega()));
                                put(USUARIO, objeto.getUsuario().getId());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Pedido objeto) throws Exception {
        int id = objeto.getId();
        linea_pedidoDAO.eliminarRegistros(objeto.getLineas_pedido());
        //Linea_pedidoDBDAO.eliminarRegistros(objeto.getLineas_pedido());
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        new Linea_pedidoDAO().eliminarRegistros(buscarPorClavePrimaria(idObjeto).getLineas_pedido());
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Pedido> obtenerLista(List<Object> listaObjetos) throws Exception {

        ArrayList<Pedido> lista = new ArrayList();
        Pedido autor;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            autor = new Pedido();
            elemento_actual = (HashMap) elemento;

            autor.setId((int) elemento_actual.get(ID));
            autor.setFecha_de_entrega(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_ENTREGA)));
            autor.setFecha_de_realizacion(DAO.obtener_fecha(elemento_actual.get(FECHA_DE_REALIZACION)));
            autor.setUsuario((Usuario) DAO.Relacion_uno_a_muchos(elemento_actual, USUARIO, usuarioDAO));
            autor.setLineas_pedido(linea_pedidoDAO.buscarLineasPedido(autor.getId()));
            lista.add(autor);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Pedido> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }
}
