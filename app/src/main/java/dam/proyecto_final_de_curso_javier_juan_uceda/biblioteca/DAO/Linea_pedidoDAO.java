package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.GenericoDAO.GenericoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Linea_pedido;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class Linea_pedidoDAO implements GenericoDAO<Linea_pedido> {

    private static XmlRpcClient Connection;
    private static PedidoDAO pedidoDAO;
    private static LibroDAO libroDAO;

    private static final String PRECIO = "precio";
    private static final String CANTIDAD = "cantidad";
    private static final String PEDIDO = "pedido_id";
    private static final String LIBRO = "libro_id";

    private static final String NOMBRE_TABLA = "biblioteca.lineapedido";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public Linea_pedidoDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();

        //libroDAO = new LibroDAO();
        if (libroDAO == null) {
            libroDAO = new LibroDAO();
        }

    }

    @Override
    public ArrayList<Linea_pedido> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, PRECIO, CANTIDAD, PEDIDO, LIBRO));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Linea_pedido buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, PRECIO, CANTIDAD, PEDIDO, LIBRO));
                    }
                }
        )));
        ArrayList<Linea_pedido> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Linea_pedido> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, PRECIO, CANTIDAD, PEDIDO, LIBRO));
                    }
                }
        )));
        ArrayList<Linea_pedido> lista = obtenerLista(list);

        return lista;
    }

    @Override
    public Integer insertarRegistro(Linea_pedido objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(PRECIO, objeto.getPrecio());
                        put(CANTIDAD, objeto.getCantidad());
                        put(PEDIDO, objeto.getPedido().getId());
                        put(LIBRO, objeto.getLibro().getId());

                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Linea_pedido objeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(
                        asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(PRECIO, objeto.getPrecio());
                                put(CANTIDAD, objeto.getCantidad());
                                put(PEDIDO, objeto.getPedido().getId());
                                put(LIBRO, objeto.getLibro().getId());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Linea_pedido objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    public void eliminarRegistros(ArrayList<Linea_pedido> lista_Lineas) throws Exception {

        for (Linea_pedido linea_pedido : lista_Lineas) {
            eliminarRegistro(linea_pedido);
        }
    }

    @Override
    public ArrayList<Linea_pedido> obtenerLista(List<Object> listaObjetos) throws Exception {

        ArrayList<Linea_pedido> lista = new ArrayList();
        Linea_pedido lineaPedido;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            lineaPedido = new Linea_pedido();
            elemento_actual = (HashMap) elemento;

            lineaPedido.setId((int) elemento_actual.get(ID));
            lineaPedido.setPrecio((double) elemento_actual.get(PRECIO));
            lineaPedido.setCantidad((int) elemento_actual.get(CANTIDAD));
            //lineaPedido.setPedido((Pedido) DAO.Relacion_uno_a_muchos(elemento_actual, PEDIDO, pedidoDAO));
            lineaPedido.setLibro((Libro) DAO.Relacion_uno_a_muchos(elemento_actual, LIBRO, libroDAO));
            lineaPedido.calcular_total();

            lista.add(lineaPedido);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Linea_pedido> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public ArrayList<Linea_pedido> buscarLineasPedido(int id) throws Exception {
        ArrayList<Linea_pedido> lineasFactura = new ArrayList<>();
        ArrayList<Linea_pedido> todasLasLineasFactura = buscarTodos();
        for (Linea_pedido lineaFactura : todasLasLineasFactura) {
            if (lineaFactura.getId() == id) {
                lineasFactura.add(lineaFactura);
            }

        }
        return lineasFactura;
    }

    public void actualizarMasDeUnRegistro(ArrayList<Linea_pedido> lineas_pedido) throws Exception {
        for (Linea_pedido lineaFactura : lineas_pedido) {
            actualizarRegistro(lineaFactura);

        }
    }

}
