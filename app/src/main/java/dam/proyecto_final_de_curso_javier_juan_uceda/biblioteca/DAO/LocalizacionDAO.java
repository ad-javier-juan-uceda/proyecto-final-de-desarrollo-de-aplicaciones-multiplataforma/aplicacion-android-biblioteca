package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO;

import android.os.Build;

import org.apache.xmlrpc.client.XmlRpcClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ConexionOdoo.ConexionOdoo;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.GenericoDAO.GenericoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Localizacion;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

import static java.util.Arrays.asList;

public class LocalizacionDAO implements GenericoDAO<Localizacion> {

    private XmlRpcClient Connection;
    private TipoDAO tipoDAO;

    private static final String NOMBRE = Constantes.NOMBRE_TIPO;
    private static final String TIPO_DEL_SITIO = "tipo_id";

    private static final String NOMBRE_TABLA = "biblioteca.localizacion";
    private static final String ID = ConexionOdoo.ID;
    private static final String FIELDS = ConexionOdoo.FIELDS;
    private static final String BUSCAR_EN_LA_TABLA = ConexionOdoo.BUSCAR_EN_LA_TABLA;
    private static final String EJECUTAR = ConexionOdoo.EJECUTAR;
    private static final String INSERTAR = ConexionOdoo.INSERTAR;
    private static final String ACTUALIZAR = ConexionOdoo.ACTUALIZAR;
    private static final String ELIMINAR = ConexionOdoo.ELIMINAR;

    public LocalizacionDAO() throws Exception {
        Connection = ConexionOdoo.getConnectionOdoo();
        tipoDAO = new TipoDAO();
    }

    @Override
    public ArrayList<Localizacion> buscarTodos() throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(Collections.emptyList()),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE));
                    }
                }
        )));

        return obtenerLista(list);
    }

    @Override
    public Localizacion buscarPorClavePrimaria(int id) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", id)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE));
                    }
                }
        )));
        ArrayList<Localizacion> lista = obtenerLista(list);

        return lista.isEmpty() ? null : lista.get(0);
    }

    @Override
    public ArrayList<Localizacion> buscarPorTodasLasClavePrimaria(ArrayList<Integer> ids) throws Exception {
        List<Object> list;

        list = asList((Object[]) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, BUSCAR_EN_LA_TABLA,
                asList(asList(
                        asList(ID, "=", ids)
                )),
                new HashMap() {
                    {
                        put(FIELDS, asList(ID, NOMBRE));
                    }
                }
        )));
        ArrayList<Localizacion> lista = obtenerLista(list);

        return lista;
    }

    public Integer insertarRegistroRestaurar(Localizacion objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());

                    }
                })
        ));

        return id;
    }

    public Integer[] insertarMasDeUnRegistroRestaurar(ArrayList<Localizacion> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistroRestaurar(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    @Override
    public Integer insertarRegistro(Localizacion objeto) throws Exception {
        final Integer id = (Integer) Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, INSERTAR,
                asList(new HashMap() {
                    {
                        put(NOMBRE, objeto.getNombre());

                    }
                })
        ));

        return id;
    }

    @Override
    public boolean actualizarRegistro(Localizacion objeto) throws Exception {

        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ACTUALIZAR,
                asList(
                        asList(objeto.getId()),
                        new HashMap() {
                            {
                                put(NOMBRE, objeto.getNombre());
                            }
                        }
                )
        ));
        return true;
    }

    @Override
    public boolean eliminarRegistro(Localizacion objeto) throws Exception {
        int id = objeto.getId();
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(id))));

        return buscarPorClavePrimaria(id) == null;
    }

    @Override
    public boolean eliminarRegistro(int idObjeto) throws Exception {
        Connection.execute(EJECUTAR, asList(
                ConexionOdoo.DB, ConexionOdoo.uid, ConexionOdoo.PASSWORD,
                NOMBRE_TABLA, ELIMINAR,
                asList(asList(idObjeto))));

        return buscarPorClavePrimaria(idObjeto) == null;
    }

    @Override
    public ArrayList<Localizacion> obtenerLista(List<Object> listaObjetos) throws Exception {
        ArrayList<Localizacion> lista = new ArrayList();
        Localizacion autor;
        HashMap elemento_actual;
        for (Object elemento : listaObjetos) {
            autor = new Localizacion();
            elemento_actual = (HashMap) elemento;

            autor.setId((int) elemento_actual.get(ID));
            autor.setNombre(DAO.transformar_texto(elemento_actual.get(NOMBRE)));

            lista.add(autor);
        }

        return lista;
    }

    @Override
    public Integer[] insertarMasDeUnRegistro(ArrayList<Localizacion> listaObjetos) throws Exception {
        int tamanyo = listaObjetos.size();
        Integer[] ids_creadas = new Integer[tamanyo];
        for (int contador = 0; contador < tamanyo; contador++) {
            ids_creadas[contador] = insertarRegistro(listaObjetos.get(contador));
        }
        return ids_creadas;
    }

    public Localizacion buscar(Localizacion objeto) throws Exception {
        Localizacion encontrado = null;
        ArrayList<Localizacion> lista = buscarTodos();
        for (int i = 0; i < lista.size(); i++) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                if (lista.get(i).equals(objeto)) {
                    i = lista.size();
                }
            }
        }
        return encontrado;
    }
}
