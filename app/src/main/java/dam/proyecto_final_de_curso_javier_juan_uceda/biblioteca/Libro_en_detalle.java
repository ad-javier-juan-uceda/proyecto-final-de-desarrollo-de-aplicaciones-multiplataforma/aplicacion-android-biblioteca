package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Date;
import java.util.Objects;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.PrestamoDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.SocioDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Prestamo;

public class Libro_en_detalle extends AppCompatActivity implements View.OnLongClickListener {
    private volatile Libro libro;
    private TextView titulo_libro;
    private TextView isbn_libro;
    private TextView estado_del_libro;
    private TextView cantidad_libro;
    private TextView paginas_del_libro;
    private TextView tipo_del_libro;
    private TextView autor_del_libro;
    private ImageView imageView2;
    private FloatingActionButton boton_flotante;
    private SwipeRefreshLayout refreshLayout;
    private Prestamo prestamo;
    private MenuItem favorito_menuitem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_libro_en_detalle);
        //libro = (Libro) getIntent().getSerializableExtra("Libro");
        //libro = (Libro) Objects.requireNonNull(getIntent().getExtras()).getSerializable("Libro");
        try {

            Thread thread2 = new Thread() {
                public void run() {
                    try {
                        libro = new LibroDAO().buscarPorClavePrimaria(getIntent().getIntExtra("Libro", 0));
                        interrupt();
                    } catch (Exception e) {

                    }
                }
            };

            thread2.start();

            while (!thread2.isInterrupted()) {

            }

        } catch (Exception e) {

        }
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setUI();

        Thread thread2 = new Thread() {
            public void run() {
                try {
                    SharedPreferences myPreferences = getSharedPreferences(MainActivity.MIS_PREFERENCIAS, MODE_PRIVATE);
                    int id_usuario = myPreferences.getInt("UsuarioID", 0);
                    if (MainActivity.socio_logueado == null && id_usuario != 0) {
                        MainActivity.socio_logueado = new SocioDAO().buscarPorClavePrimaria(id_usuario);
                    }
                } catch (Exception e) {

                }
            }
        };
        thread2.start();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.favorito_menuitem:
                /*if (hay_libro(MainActivity.socio_logueado.getLista_libros_seguimiento(), libro)) {
                    MainActivity.socio_logueado.setLista_libros_seguimiento(eliminar_libro(MainActivity.socio_logueado.getLista_libros_seguimiento(), libro));
                } else {
                    MainActivity.socio_logueado.getLista_libros_seguimiento().add(libro);
                }
                MainActivity.libros_favoritos.reiniciarthreads(MainActivity.socio_logueado.getLista_libros_seguimiento(), getApplicationContext());
                Thread thread = new Thread() {
                    public void run() {

                        try {
                            new SocioDAO().actualizarRegistro(MainActivity.socio_logueado);
                        } catch (Exception e) {

                        }
                        runOnUiThread(() -> {
                            poner_texto();
                            if (hay_libro(MainActivity.socio_logueado.getLista_libros_seguimiento(), libro)) {
                                favorito_menuitem.setIcon(android.R.drawable.star_big_on);
                                favorito_menuitem.setTitle("Eliminar de favoritos");
                            } else {
                                favorito_menuitem.setIcon(android.R.drawable.star_big_off);
                                favorito_menuitem.setTitle("Guardar como favoritos");
                            }
                        });
                    }
                };
                thread.start();

*/
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.libros, menu);
        favorito_menuitem = menu.findItem(R.id.favorito_menuitem);

        /*if (hay_libro(MainActivity.socio_logueado.getLista_libros_seguimiento(), libro)) {
            favorito_menuitem.setIcon(android.R.drawable.star_big_on);
            favorito_menuitem.setTitle("Eliminar de favoritos");
        } else {
            favorito_menuitem.setIcon(android.R.drawable.star_big_off);
            favorito_menuitem.setTitle("Guardar como favoritos");
        }*/

        return true;
    }


    private void setUI() {
        titulo_libro = findViewById(R.id.titulo_libro);
        isbn_libro = findViewById(R.id.isbn_libro);
        estado_del_libro = findViewById(R.id.estado_del_libro);
        cantidad_libro = findViewById(R.id.cantidad_libro);
        paginas_del_libro = findViewById(R.id.paginas_del_libro);
        tipo_del_libro = findViewById(R.id.tipo_del_libro);
        autor_del_libro = findViewById(R.id.autor_del_libro);
        imageView2 = findViewById(R.id.imageView2);
        boton_flotante = findViewById(R.id.boton_flotante);

        poner_texto();


    }

    private void poner_texto() {
        titulo_libro.setText(libro.getTitulo());
        isbn_libro.setText(libro.getISBN());

        int cantidad = libro.getCantidad();
        String texto_cantidad = "";
        if (cantidad > 1) {
            texto_cantidad = getString(R.string.Queda) + cantidad + getString(R.string.libro);
        } else {
            texto_cantidad = getString(R.string.Quedan) + cantidad + getString(R.string.libros);
        }

        estado_del_libro.setText(libro.getEstado_del_libro());
        cantidad_libro.setText(texto_cantidad);

        int paginas = libro.getCantidad();
        String texto_paginas = "";
        if (paginas > 1) {
            texto_paginas = getString(R.string.Queda) + cantidad + getString(R.string.pagina);
        } else {
            texto_paginas = getString(R.string.Quedan) + cantidad + getString(R.string.paginas);
        }

        paginas_del_libro.setText(texto_paginas);
        tipo_del_libro.setText(libro.getTipo_del_libro().getNombre());
        autor_del_libro.setText(libro.getAutor().getNombre());

        imageView2.setImageResource(libro.isEsta_prestado() ? android.R.drawable.star_big_on : android.R.drawable.star_big_off);

        refreshLayout = findViewById(R.id.refresh_layout);

        refreshLayout.setOnRefreshListener(
                () -> {

                    Thread thread = new Thread() {
                        public void run() {
                            refreshLayout.setRefreshing(true);
                            try {
                                libro = new LibroDAO().buscarPorClavePrimaria(libro.getId());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            runOnUiThread(() -> poner_texto());

                            if (refreshLayout.isRefreshing()) {
                                refreshLayout.setRefreshing(false);
                            }
                        }
                    };
                    thread.start();

                }
        );

        refreshLayout.setColorSchemeResources(
                R.color.color1azul,
                R.color.color2amarillo,
                R.color.color3verde,
                R.color.color4naranja,
                R.color.color5negro,
                R.color.color6violeta,
                R.color.color7marron,
                R.color.color8rosa,
                R.color.color9fucsia,
                R.color.color10rojo
        );

        boton_flotante.setOnLongClickListener(this);


        boton_flotante.setOnClickListener(view -> {
            try {
                if (libro != null) {
                    Intent intent = new Intent(getApplicationContext(), crear_prestamo.class);
                    intent.putExtra("Libro", libro.getId());
                    startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    public void styleSnackbar(View view, Prestamo prestamo) {
        Snackbar snackbar = Snackbar.make(view, getString(R.string.preguntaDeshacerInsercion), Snackbar.LENGTH_LONG).setAction("YES", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Thread thread = new Thread() {
                        public void run() {

                            try {
                                libro.setEsta_prestado(false);
                                prestamo.setEsta_devuelto(true);
                                prestamo.setFecha_que_se_ha_devuelto(new Date());
                                new PrestamoDAO().actualizarRegistro(prestamo);
                                new LibroDAO().actualizarRegistro(libro);
                                crear_toast("Libro devuelto", true);
                            } catch (Exception e) {

                            }

                        }
                    };
                    thread.start();
                } catch (Exception e) {
                    crear_toast(getString(R.string.libro_no_devuelto) + e.toString() + " " + e.getMessage(), false);
                }
            }
        });

        //ACTION
        snackbar.setActionTextColor(getResources().getColor(R.color.color1azul));

        View snackBarView = snackbar.getView();
        //BACKGROUND
        snackBarView.setBackgroundColor(getResources().getColor(R.color.color3verde));

        TextView tv = snackbar.getView().findViewById(R.id.snackbar_text);
        tv.setTextColor(Color.RED);

        snackbar.show();
    }

    public void crear_toast(final String text_to_print, final boolean acierto) {

        runOnUiThread(() -> {
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate((acierto ? R.layout.toast_personalizado_acierto : R.layout.toast_personalizado_error), findViewById(R.id.toast_personalizado));

            TextView text = layout.findViewById(R.id.textView);
            text.setText(text_to_print);

            android.widget.Toast toast = new android.widget.Toast(getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(android.widget.Toast.LENGTH_LONG);
            toast.setView(layout);
            toast.show();
        });


    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.boton_flotante:
                /*if (prestamo != null) {
                    if (prestamo.getSocio().equals(MainActivity.socio_logueado)) {
                        if (libro.isEsta_prestado()) {
                            Intent intent = new Intent(getApplicationContext(), crear_prestamo.class);
                            intent.putExtra("libro", libro);
                            intent.putExtra("prestamo", prestamo);
                            startActivity(intent);
                        }
                    }
                }*/
                break;
            default:
                break;
        }
        return false;
    }
}
