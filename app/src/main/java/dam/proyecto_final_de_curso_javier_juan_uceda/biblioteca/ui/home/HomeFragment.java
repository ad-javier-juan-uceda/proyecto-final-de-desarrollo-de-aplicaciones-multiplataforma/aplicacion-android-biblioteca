package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ui.home;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import java.util.ArrayList;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.LibroDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Inicio;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Libro_en_detalle;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.MainActivity;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.R;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView.Adaptador;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Threads.ThreadLibro;

import static dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.MainActivity.misPreferencias;

public class HomeFragment extends Fragment implements Adaptador.OnItemClickListener,
        View.OnClickListener, MenuItem.OnMenuItemClickListener {

    private HomeViewModel homeViewModel;
    private View root;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private Context context;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private SearchView searchView = null;
    private SearchView.OnQueryTextListener queryTextListener;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        try {
            homeViewModel = ViewModelProviders.of(this).get(HomeViewModel.class);
            root = inflater.inflate(R.layout.fragment_home, container, false);
            final TextView textView = root.findViewById(R.id.text_home);
            context = this.getContext();
            recyclerView = root.findViewById(R.id.recyclerViewLibros);
            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getContext()));
            Adaptador adaptador = new Adaptador(context, ThreadLibro.libros, ((activityName) -> Click(activityName)));
            recyclerView.setAdapter(adaptador);

            refreshLayout = root.findViewById(R.id.refresh_layout);

            refreshLayout.setOnRefreshListener(
                    () -> {

                        Thread thread = new Thread() {
                            public void run() {
                                refreshLayout.setRefreshing(true);

                                try {
                                    if (new MainActivity().isNetworkAvailable()) {
                                        ThreadLibro.libros.clear();
                                        ThreadLibro.libros.addAll(new LibroDAO().buscarTodos());
                                    }

                                } catch (Exception e) {

                                }

                                if (refreshLayout.isRefreshing()) {
                                    refreshLayout.setRefreshing(false);
                                }

                                try {
                                    getActivity().runOnUiThread(() -> {
                                        Adaptador adaptador1 = new Adaptador(context, ThreadLibro.libros, ((activityName) -> Click(activityName)));
                                        recyclerView.setAdapter(adaptador1);
                                    });
                                } catch (NullPointerException n) {

                                }
                                interrupt();
                            }
                        };
                        thread.start();
                    }
            );

            refreshLayout.setColorSchemeResources(
                    R.color.color1azul,
                    R.color.color2amarillo,
                    R.color.color3verde,
                    R.color.color4naranja,
                    R.color.color5negro,
                    R.color.color6violeta,
                    R.color.color7marron,
                    R.color.color8rosa,
                    R.color.color9fucsia,
                    R.color.color10rojo
            );

            homeViewModel.getText().observe(this.getViewLifecycleOwner(), s -> textView.setText(s));
        } catch (Exception e) {

        }

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.navegador, menu);
        MenuItem cerrar_sesion = menu.findItem(R.id.action_settings);
        cerrar_sesion.setOnMenuItemClickListener(this);
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if (searchView != null) {
            searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    return true;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    filtrar(newText);
                    return true;
                }
            });
        }
    }

    private void filtrar(String newText) {
        ArrayList<Libro> lista_libros = new ArrayList();
        for (Libro libro : ThreadLibro.libros) {
            if (libro.getTitulo().toLowerCase().contains(newText)) {
                lista_libros.add(libro);
            }
        }
        Adaptador adaptador1 = new Adaptador(context, lista_libros, ((activityName) -> Click(activityName)));
        recyclerView.setAdapter(adaptador1);
    }

    @Override
    public void Click(Libro activityName) {
        Intent intent = new Intent(root.getContext(), Libro_en_detalle.class);
        intent.putExtra("Libro", activityName.getId());
        startActivity(intent);
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:

                startActivity(new Intent(getActivity().getBaseContext(), Inicio.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP));
                MainActivity.socio_logueado = null;
                MainActivity.escribir_en_preferencias(0);
                SharedPreferences.Editor editor = misPreferencias.edit();
                editor.putInt("UsuarioID", 0);
                editor.apply();

                getActivity().finish();
                break;
        }
        return false;
    }
}