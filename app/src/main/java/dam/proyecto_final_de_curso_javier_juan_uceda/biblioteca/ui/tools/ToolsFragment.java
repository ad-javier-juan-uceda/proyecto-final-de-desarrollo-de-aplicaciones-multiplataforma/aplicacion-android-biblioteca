package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ui.tools;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.SocioDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.MainActivity;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.R;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.DAO;

public class ToolsFragment extends Fragment implements View.OnClickListener {

    private ToolsViewModel toolsViewModel;

    private TextView tvnombreusuario;
    private TextView tvpantallasesion;
    private TextView tvcontrasenya;
    private EditText ednombredeusuario;
    private EditText edcontrasenya;
    private Button btcontrasenya;
    private ProgressBar progressBar;
    private ImageView contrasenya;
    private ImageView imageView6;
    private EditText edcontrasenya2;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        toolsViewModel = ViewModelProviders.of(this).get(ToolsViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tools, container, false);
        final TextView textView = root.findViewById(R.id.text_tools);
        toolsViewModel.getText().observe(getViewLifecycleOwner(), s -> textView.setText(s));

        try {
            tvnombreusuario = root.findViewById(R.id.tvnombreusuario);
            tvpantallasesion = root.findViewById(R.id.tvpantallasesion);
            tvcontrasenya = root.findViewById(R.id.tvcontrasenya);
            ednombredeusuario = root.findViewById(R.id.ednombredeusuario);
            edcontrasenya = root.findViewById(R.id.edContrasenya);
            edcontrasenya2 = root.findViewById(R.id.edcontrasenya2);
            btcontrasenya = root.findViewById(R.id.btcontrasenya);
            btcontrasenya.setOnClickListener(this);
            progressBar = root.findViewById(R.id.progressBar);
            contrasenya = root.findViewById(R.id.imagencontrasenyacambiar1);
            imageView6 = root.findViewById(R.id.imageView6);
            imageView6.setOnClickListener(this);
            edcontrasenya2.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    edcontrasenya2.setBackgroundColor((edcontrasenya.getText().toString().equals(edcontrasenya2.getText().toString())) ? Color.GREEN : Color.RED);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }

            });
            contrasenya.setOnClickListener(this);
            edcontrasenya.setText(MainActivity.socio_logueado.getDNI());
            ednombredeusuario.setText(MainActivity.socio_logueado.getNombre());
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage() + " " + e.toString(), Toast.LENGTH_LONG).show();
        }


        return root;
    }

    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {
                case R.id.imageView6:
                    edcontrasenya2.setInputType(edcontrasenya2.getInputType() == 129 ? InputType.TYPE_CLASS_TEXT : 129);
                    imageView6.setImageResource(edcontrasenya2.getInputType() == 129 ? android.R.drawable.ic_menu_edit : android.R.drawable.ic_menu_view);
                    break;
                case R.id.imagencontrasenyacambiar1:
                    edcontrasenya.setInputType(edcontrasenya.getInputType() == 129 ? InputType.TYPE_CLASS_TEXT : 129);
                    contrasenya.setImageResource(edcontrasenya.getInputType() == 129 ? android.R.drawable.ic_menu_edit : android.R.drawable.ic_menu_view);
                    break;

                case R.id.btcontrasenya:
                    actualizarsocio();
                    break;
                default:
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage() + " " + e.toString(), Toast.LENGTH_LONG).show();
        }

    }

    private void actualizarsocio() {

        if (edcontrasenya.getText().toString().equals(edcontrasenya2.getText().toString())) {
            Thread thread = new Thread() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                public void run() {
                    if (new MainActivity().isNetworkAvailable()) {
                        MainActivity.socio_logueado.setNombre(ednombredeusuario.getText().toString());
                        MainActivity.socio_logueado.setContrasenya(DAO.desencriptar_contrasenya(edcontrasenya2.getText().toString()));
                        try {
                            new SocioDAO().actualizarRegistro(MainActivity.socio_logueado);
                        } catch (Exception e) {

                        }
                    }
                }
            };
            thread.start();
        }
    }
}