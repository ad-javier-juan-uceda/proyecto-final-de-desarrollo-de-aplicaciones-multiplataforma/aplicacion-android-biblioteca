package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.ui.gallery;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.DAO.SocioDAO;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.MainActivity;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo.Libro;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.R;
import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.RecyclerView.Adaptador;

public class GalleryFragment extends Fragment implements Adaptador.OnItemClickListener,
        View.OnClickListener {

    private GalleryViewModel galleryViewModel;

    private View root;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout refreshLayout;
    private Context context;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        try {
            galleryViewModel = ViewModelProviders.of(this).get(GalleryViewModel.class);
            root = inflater.inflate(R.layout.fragment_gallery, container, false);
            final TextView textView = root.findViewById(R.id.text_home);
            context = this.getContext();
            refreshLayout.setOnRefreshListener(
                    () -> {

                        Thread thread = new Thread() {
                            public void run() {
                                refreshLayout.setRefreshing(true);
                                try {
                                    if (new MainActivity().isNetworkAvailable()) {
                                        MainActivity.socio_logueado = new SocioDAO().buscarPorClavePrimaria(MainActivity.socio_logueado.getId());
                                    }
                                } catch (Exception e) {

                                }
                                if (refreshLayout.isRefreshing()) {
                                    refreshLayout.setRefreshing(false);
                                }
                                interrupt();
                            }
                        };
                        thread.start();
                    }
            );

            refreshLayout.setColorSchemeResources(
                    R.color.color1azul,
                    R.color.color2amarillo,
                    R.color.color3verde,
                    R.color.color4naranja,
                    R.color.color5negro,
                    R.color.color6violeta,
                    R.color.color7marron,
                    R.color.color8rosa,
                    R.color.color9fucsia,
                    R.color.color10rojo
            );

            galleryViewModel.getText().observe(this.getViewLifecycleOwner(), s -> textView.setText(s));
        } catch (Exception e) {

        }

        return root;
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void Click(Libro activityName) {
        /*Intent intent = new Intent(root.getContext(), Libro_en_detalle.class);
        intent.putExtra("Libro", activityName);
        startActivity(intent);*/
    }
}