package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Objects;

public class Linea_pedido {

    private Pedido pedido;
    private Libro libro;
    private int cantidad;
    private double total_linea;
    private double precio;
    private int id;

    public Linea_pedido(Pedido pedido, Libro libro, int cantidad, double total_linea, double precio) {
        this.pedido = pedido;
        this.libro = libro;
        this.cantidad = cantidad;
        this.total_linea = total_linea;
        this.precio = precio;
    }

    public Linea_pedido() {
        this.pedido = new Pedido();
        this.libro = new Libro();
        this.cantidad = 0;
        this.total_linea = 0.0;
        this.id = 0;
        this.precio = 0.0;
    }

    public Linea_pedido(Linea_pedido lineaPedido) {
        this.id = lineaPedido.getId();
        this.pedido = lineaPedido.getPedido();
        this.libro = lineaPedido.getLibro();
        this.cantidad = lineaPedido.getCantidad();
        this.total_linea = lineaPedido.getTotal_linea();
        id = lineaPedido.getId();
        this.precio = lineaPedido.getPrecio();
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void calcular_total() {
        setTotal_linea(getPrecio() * getCantidad());
    }

    public Pedido getPedido() {
        return pedido;
    }

    public void setPedido(Pedido pedido) {
        this.pedido = pedido;
    }

    public Libro getLibro() {
        return libro;
    }

    public void setLibro(Libro libro) {
        this.libro = libro;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getTotal_linea() {
        return total_linea;
    }

    public void setTotal_linea(double total_linea) {
        this.total_linea = total_linea;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Linea_pedido other = (Linea_pedido) obj;
        if (this.cantidad != other.cantidad) {
            return false;
        }
        if (Double.doubleToLongBits(this.total_linea) != Double.doubleToLongBits(other.total_linea)) {
            return false;
        }
        if (!Objects.equals(this.pedido, other.pedido)) {
            return false;
        }
        return Objects.equals(this.libro, other.libro);
    }

    public boolean Equals(Linea_pedido objeto) {
        boolean igual = true;
        try {
            if (!(
                    objeto.getLibro().equals(this.getLibro())
                            && objeto.getCantidad() == this.getCantidad()
                            && objeto.getPrecio() == this.getPrecio()
                            && objeto.getTotal_linea() == this.getTotal_linea()
                            && objeto.getPedido().Equals(this.getPedido())
            )) {
                throw new Exception();
            }
        } catch (Exception e) {
            igual = false;
        }
        return igual;
    }


    @Override
    public String toString() {
        return libro.toString();
    }

}
