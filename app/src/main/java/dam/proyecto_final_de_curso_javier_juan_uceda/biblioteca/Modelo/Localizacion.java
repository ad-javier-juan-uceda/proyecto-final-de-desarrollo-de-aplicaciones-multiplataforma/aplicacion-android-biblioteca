package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo;


import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.Objects;

import dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Utilidades.Constantes;

public class Localizacion {

    private String nombre;
    private int id;

    public Localizacion(String nombre) {
        this.nombre = nombre;
    }

    public Localizacion() {
        this.nombre = Constantes.VACIO;
        id = 0;
    }

    public Localizacion(Localizacion localizacion) {
        this.nombre = localizacion.getNombre();
        id = localizacion.getId();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Localizacion other = (Localizacion) obj;
        return Objects.equals(this.nombre, other.nombre);
    }


    public boolean equals(Localizacion objeto) {
        boolean igual = true;
        try {
            if (!(objeto.getNombre().equals(this.getNombre()))) {
                throw new Exception();
            }
        } catch (Exception e) {
            igual = false;
        }
        return igual;
    }

    @Override
    public String toString() {
        return nombre;
    }

}
