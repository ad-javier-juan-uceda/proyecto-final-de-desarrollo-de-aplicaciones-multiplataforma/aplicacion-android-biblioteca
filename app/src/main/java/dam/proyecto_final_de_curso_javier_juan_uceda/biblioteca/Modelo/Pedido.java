package dam.proyecto_final_de_curso_javier_juan_uceda.biblioteca.Modelo;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;

public class Pedido {

    private Date fecha_de_realizacion;
    private Date fecha_de_entrega;
    private ArrayList<Linea_pedido> lineas_pedido;
    private double total_pedido;
    private int id;
    private Usuario usuario;
    public Pedido(Date fecha_de_realizacion, Date fecha_de_entrega, ArrayList<Linea_pedido> lineas_pedido, Usuario usuario) {
        this.fecha_de_realizacion = fecha_de_realizacion;
        this.fecha_de_entrega = fecha_de_entrega;
        this.lineas_pedido = lineas_pedido;
        total_pedido = 0.0;
        this.usuario = usuario;

    }

    public Pedido() {
        this.fecha_de_realizacion = new Date();
        this.fecha_de_entrega = new Date();
        this.lineas_pedido = new ArrayList();
        this.usuario = new Usuario();
        total_pedido = 0.0;
        id = 0;

    }

    public Pedido(Pedido pedido) {
        this.fecha_de_realizacion = pedido.getFecha_de_realizacion();
        this.fecha_de_entrega = pedido.getFecha_de_entrega();
        this.lineas_pedido = pedido.getLineas_pedido();
        total_pedido = pedido.getTotal_pedido();
        id = pedido.getId();
        usuario = pedido.getUsuario();
    }

    public Date getFecha_de_realizacion() {
        return fecha_de_realizacion;
    }

    public void setFecha_de_realizacion(Date fecha_de_realizacion) {
        this.fecha_de_realizacion = fecha_de_realizacion;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }


    public Date getFecha_de_entrega() {
        return fecha_de_entrega;
    }

    public void setFecha_de_entrega(Date fecha_de_entrega) {
        this.fecha_de_entrega = fecha_de_entrega;
    }

    public void calcular_total() {
        double total = 0.0;
        for (Linea_pedido linea : getLineas_pedido()) {
            total += linea.getTotal_linea();
        }
        setTotal_pedido(total);
    }

    public ArrayList<Linea_pedido> getLineas_pedido() {
        return lineas_pedido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLineas_pedido(ArrayList<Linea_pedido> lineas_pedido) {
        this.lineas_pedido = lineas_pedido;
    }

    public double getTotal_pedido() {
        return total_pedido;
    }

    public void setTotal_pedido(double total_pedido) {
        this.total_pedido = total_pedido;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Pedido other = (Pedido) obj;
        if (Double.doubleToLongBits(this.total_pedido) != Double.doubleToLongBits(other.total_pedido)) {
            return false;
        }
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_realizacion, other.fecha_de_realizacion)) {
            return false;
        }
        if (!Objects.equals(this.fecha_de_entrega, other.fecha_de_entrega)) {
            return false;
        }
        if (!Objects.equals(this.lineas_pedido, other.lineas_pedido)) {
            return false;
        }
        return Objects.equals(this.usuario, other.usuario);
    }

    public boolean Equals(Pedido objeto) {
        boolean igual = true;
//        System.out.println(objeto.getFecha_de_entrega() + " " + this.getFecha_de_entrega());
//        System.out.println(objeto.getFecha_de_realizacion() + " " + this.getFecha_de_realizacion());
//        System.out.println(objeto.getUsuario() + " " + this.getUsuario().getNombre());
        try {
            if (!(
                    objeto.getFecha_de_entrega().getDate() == this.getFecha_de_entrega().getDate() &&
                            objeto.getFecha_de_entrega().getMonth() == this.getFecha_de_entrega().getMonth() &&
                            objeto.getFecha_de_entrega().getYear() == this.getFecha_de_entrega().getYear() &&
                            objeto.getFecha_de_realizacion().getDate() == this.getFecha_de_realizacion().getDate() &&
                            objeto.getFecha_de_realizacion().getMonth() == this.getFecha_de_realizacion().getMonth() &&
                            objeto.getFecha_de_realizacion().getYear() == this.getFecha_de_realizacion().getYear()
                            && objeto.getUsuario().Equals(this.getUsuario())
            )) {
                throw new Exception();
            }

//            for (int contador = 0; contador < objeto.getLineas_pedido().size(); contador++){
//                if (!objeto.getLineas_pedido().get(contador).Equals(this.getLineas_pedido().get(contador))){
//                    throw new Exception();
//                }
//            }
        } catch (Exception e) {
            igual = false;
        }
        return igual;
    }

    @Override
    public String toString() {
        return fecha_de_realizacion + " - " + fecha_de_entrega;
    }

}
